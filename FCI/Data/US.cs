﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;
using System.IO;

namespace FCI.Data
{
    static class US // user setting class
    {

        private static JObject jsonObject = null;
        public static int minLate;
        public static int minEarly;
        public static int cameraID;
        public static int threshold;
        public static int numCap;

        public static void init_US()
        {
            try
            {
                var jsonString = File.Exists(Program.settingFile) ? File.ReadAllText(Program.settingFile) : "{}";
                jsonObject = JObject.Parse(jsonString);
                minLate = int.Parse(jsonObject["minLate"].ToString());
                minEarly = int.Parse(jsonObject["minEarly"].ToString());
                cameraID = int.Parse(jsonObject["cameraID"].ToString());
                threshold = int.Parse(jsonObject["threshold"].ToString());
                numCap = int.Parse(jsonObject["numCap"].ToString());
            }
            catch
            {
                minLate = 5;
                minEarly = 30;
                cameraID = 0;
                threshold = 70;
                numCap = 10;
            }
            saveChange();
        }

        public static bool saveChange(int inp_minLate = -1, int inp_minEarly = -1, int inp_cameraID = -1, int inp_threshold = -1, int inp_numCap =-1)
        {
            try
            {
                jsonObject["minLate"] = minLate = inp_minLate == -1 ? minLate : inp_minLate;
                jsonObject["minEarly"] = minEarly = inp_minEarly == -1 ? minEarly : inp_minEarly;
                jsonObject["cameraID"] = cameraID = inp_cameraID == -1 ? cameraID : inp_cameraID;
                jsonObject["threshold"] = threshold = inp_threshold  == -1 ? threshold : inp_threshold;
                jsonObject["numCap"] = numCap  = inp_numCap  == -1 ? numCap : inp_numCap;
                File.WriteAllText(Program.settingFile, jsonObject.ToString());
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}
