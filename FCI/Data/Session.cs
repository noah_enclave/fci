﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FCI.Data
{
    static class Session
    {
        public static int idAccount { get; set; }
        public static int? idRole { get; set; }
        public static string RoleName { get; set; }
        public static string idInstructor { get; set; }
        public static string I_Name { get; set; }
        public static string I_idDepartment { get; set; }
        public static string DepartmentName { get; set; }
        public static string FacultyName { get; set; }
        public static void setSession(int a, int? r)
        {
            idAccount = a;
            idRole = r;
        }

        public static void clearSession()
        {
            idAccount = -1;
            idRole = -1;
        }

        public static void setInstructor(string i, string n, string d)
        {
            idInstructor = i;
            I_Name = n;
            I_idDepartment = d;
            var tmpd = Program.db.Department.FirstOrDefault(dep => dep.idDepartment == d);
            string idF = tmpd == null ? "admin" : tmpd.idFaculty;
            var tmpf = Program.db.Faculty.FirstOrDefault(f => f.idFaculty == idF);
            FacultyName = tmpf == null ? "" : tmpf.FacultyName;
        }
        public static string getSession()
        {
            return
                //"* ID: " + idInstructor + Environment.NewLine +
                "- Họ tên: " + Environment.NewLine + I_Name + Environment.NewLine +
                "- Chức vụ: " + Environment.NewLine + RoleName + Environment.NewLine +
                (
                    (I_idDepartment== "admin") ?"":
                    ("- Khoa: " + Environment.NewLine + FacultyName + Environment.NewLine +
                    "- Bộ môn: " + Environment.NewLine + DepartmentName)
                );
            ;
        }
    }
}
