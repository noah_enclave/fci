﻿using FCI.Data;
using System;
using System.Threading;
using System.Windows.Forms;
using System.Linq;
using System.Collections.Generic;

namespace FCI
{
    static class Program
    {
        public static FCI_DBEntities db = null;
        public static string faceDir, faceIndex, faceIndexFile, settingFile;
        public static Emgu.CV.CascadeClassifier faceLib = null;
        public static Emgu.CV.Capture grabber = null;
        public static View.TrackingCamera trackingcamera = null;
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            init_value();
            login();

            //test
            //Data.Session.idAccount = 11;
            //Application.Run(new View.Tracking());
            //Application.Run(new View.SystemSetting());
            //Data.Session.idAccount = 2;
            //Application.Run(new View.Learning());
            //Application.Run(new View.Tracking());
            //Application.Run(new View.SystemSetting());
        }

        public static bool login(bool run = true)
        {
            if ((new View.Accessing()).ShowDialog() != DialogResult.OK)
            {
                Application.Exit();
            }
            else if (run)
            {
                Application.Run(new View.Main());
            }
            else
            {
                return true;
            }
            return false;
        }

        static void init_value()
        {
            trackingcamera = new View.TrackingCamera();
            db = new FCI_DBEntities();
            faceDir = Application.StartupPath + @"\DataFaces\";
            faceIndex = "index.json";
            faceIndexFile = faceDir + faceIndex;
            settingFile = Application.StartupPath + @"\setting.json";
            faceLib = new Emgu.CV.CascadeClassifier(Application.StartupPath + @"\Cascades\haarcascade_frontalface_alt2.xml");
            US.init_US();
        }

    }
}
