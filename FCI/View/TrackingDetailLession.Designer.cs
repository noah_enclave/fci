﻿namespace FCI.View
{
    partial class TrackingDetailLession
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TrackingDetailLession));
            this.pictureBox_avatar = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label_vangtong = new System.Windows.Forms.Label();
            this.label_muonhoc = new System.Windows.Forms.Label();
            this.label_sdt = new System.Windows.Forms.Label();
            this.label_hoten = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox_hinhanh = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label_tietthu = new System.Windows.Forms.Label();
            this.label_batdau = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label_ketthuc = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label_diemdanh = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label_ngay = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label_trangthai = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_avatar)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_hinhanh)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox_avatar
            // 
            this.pictureBox_avatar.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pictureBox_avatar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_avatar.Location = new System.Drawing.Point(12, 12);
            this.pictureBox_avatar.Name = "pictureBox_avatar";
            this.pictureBox_avatar.Size = new System.Drawing.Size(130, 130);
            this.pictureBox_avatar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_avatar.TabIndex = 0;
            this.pictureBox_avatar.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label_vangtong);
            this.panel2.Controls.Add(this.label_muonhoc);
            this.panel2.Controls.Add(this.label_sdt);
            this.panel2.Controls.Add(this.label_hoten);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.panel2.Location = new System.Drawing.Point(148, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(340, 130);
            this.panel2.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label4.Location = new System.Drawing.Point(3, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(130, 15);
            this.label4.TabIndex = 0;
            this.label4.Text = "Số buổi vắng / hiện tại:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label3.Location = new System.Drawing.Point(3, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(142, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "Số buổi đi muộn / đi học:";
            // 
            // label_vangtong
            // 
            this.label_vangtong.AutoSize = true;
            this.label_vangtong.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label_vangtong.Location = new System.Drawing.Point(146, 66);
            this.label_vangtong.Name = "label_vangtong";
            this.label_vangtong.Size = new System.Drawing.Size(14, 15);
            this.label_vangtong.TabIndex = 0;
            this.label_vangtong.Text = "_";
            // 
            // label_muonhoc
            // 
            this.label_muonhoc.AutoSize = true;
            this.label_muonhoc.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label_muonhoc.Location = new System.Drawing.Point(146, 94);
            this.label_muonhoc.Name = "label_muonhoc";
            this.label_muonhoc.Size = new System.Drawing.Size(14, 15);
            this.label_muonhoc.TabIndex = 0;
            this.label_muonhoc.Text = "_";
            // 
            // label_sdt
            // 
            this.label_sdt.AutoSize = true;
            this.label_sdt.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label_sdt.Location = new System.Drawing.Point(146, 41);
            this.label_sdt.Name = "label_sdt";
            this.label_sdt.Size = new System.Drawing.Size(14, 15);
            this.label_sdt.TabIndex = 0;
            this.label_sdt.Text = "_";
            // 
            // label_hoten
            // 
            this.label_hoten.AutoSize = true;
            this.label_hoten.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label_hoten.Location = new System.Drawing.Point(146, 16);
            this.label_hoten.Name = "label_hoten";
            this.label_hoten.Size = new System.Drawing.Size(14, 15);
            this.label_hoten.TabIndex = 0;
            this.label_hoten.Text = "_";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label15.Location = new System.Drawing.Point(3, 41);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(82, 15);
            this.label15.TabIndex = 0;
            this.label15.Text = "Số điện thoại:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label2.Location = new System.Drawing.Point(3, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "Họ Tên Sinh Viên:";
            // 
            // pictureBox_hinhanh
            // 
            this.pictureBox_hinhanh.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pictureBox_hinhanh.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_hinhanh.InitialImage = null;
            this.pictureBox_hinhanh.Location = new System.Drawing.Point(33, 260);
            this.pictureBox_hinhanh.Name = "pictureBox_hinhanh";
            this.pictureBox_hinhanh.Size = new System.Drawing.Size(352, 250);
            this.pictureBox_hinhanh.TabIndex = 0;
            this.pictureBox_hinhanh.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label1.Location = new System.Drawing.Point(31, 150);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tiết thứ:";
            // 
            // label_tietthu
            // 
            this.label_tietthu.AutoSize = true;
            this.label_tietthu.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label_tietthu.Location = new System.Drawing.Point(127, 150);
            this.label_tietthu.Name = "label_tietthu";
            this.label_tietthu.Size = new System.Drawing.Size(14, 15);
            this.label_tietthu.TabIndex = 0;
            this.label_tietthu.Text = "_";
            // 
            // label_batdau
            // 
            this.label_batdau.AutoSize = true;
            this.label_batdau.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label_batdau.Location = new System.Drawing.Point(127, 180);
            this.label_batdau.Name = "label_batdau";
            this.label_batdau.Size = new System.Drawing.Size(14, 15);
            this.label_batdau.TabIndex = 0;
            this.label_batdau.Text = "_";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label7.Location = new System.Drawing.Point(31, 180);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 15);
            this.label7.TabIndex = 0;
            this.label7.Text = "Giờ bắt đầu:";
            // 
            // label_ketthuc
            // 
            this.label_ketthuc.AutoSize = true;
            this.label_ketthuc.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label_ketthuc.Location = new System.Drawing.Point(339, 180);
            this.label_ketthuc.Name = "label_ketthuc";
            this.label_ketthuc.Size = new System.Drawing.Size(14, 15);
            this.label_ketthuc.TabIndex = 0;
            this.label_ketthuc.Text = "_";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label9.Location = new System.Drawing.Point(262, 180);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 15);
            this.label9.TabIndex = 0;
            this.label9.Text = "Giờ kết thúc:";
            // 
            // label_diemdanh
            // 
            this.label_diemdanh.AutoSize = true;
            this.label_diemdanh.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label_diemdanh.Location = new System.Drawing.Point(127, 207);
            this.label_diemdanh.Name = "label_diemdanh";
            this.label_diemdanh.Size = new System.Drawing.Size(14, 15);
            this.label_diemdanh.TabIndex = 0;
            this.label_diemdanh.Text = "_";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label11.Location = new System.Drawing.Point(31, 207);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 15);
            this.label11.TabIndex = 0;
            this.label11.Text = "Giờ điểm danh:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label14.Location = new System.Drawing.Point(30, 242);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(108, 15);
            this.label14.TabIndex = 0;
            this.label14.Text = "Hình ảnh xác thực:";
            // 
            // label_ngay
            // 
            this.label_ngay.AutoSize = true;
            this.label_ngay.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label_ngay.Location = new System.Drawing.Point(339, 150);
            this.label_ngay.Name = "label_ngay";
            this.label_ngay.Size = new System.Drawing.Size(14, 15);
            this.label_ngay.TabIndex = 0;
            this.label_ngay.Text = "_";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label13.Location = new System.Drawing.Point(262, 150);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(38, 15);
            this.label13.TabIndex = 0;
            this.label13.Text = "Ngày:";
            // 
            // label_trangthai
            // 
            this.label_trangthai.AutoSize = true;
            this.label_trangthai.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label_trangthai.Location = new System.Drawing.Point(262, 207);
            this.label_trangthai.Name = "label_trangthai";
            this.label_trangthai.Size = new System.Drawing.Size(14, 15);
            this.label_trangthai.TabIndex = 0;
            this.label_trangthai.Text = "_";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(402, 412);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Lưu hình";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(402, 441);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 44);
            this.button2.TabIndex = 1;
            this.button2.Text = "Đóng";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // TrackingDetailLession
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 522);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pictureBox_hinhanh);
            this.Controls.Add(this.pictureBox_avatar);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label_diemdanh);
            this.Controls.Add(this.label_trangthai);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label_ketthuc);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label_batdau);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label_ngay);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label_tietthu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TrackingDetailLession";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Xem chi tiết";
            this.Load += new System.EventHandler(this.TrackingDetailLession_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_avatar)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_hinhanh)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox_avatar;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label_vangtong;
        private System.Windows.Forms.Label label_muonhoc;
        private System.Windows.Forms.Label label_hoten;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox_hinhanh;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_tietthu;
        private System.Windows.Forms.Label label_batdau;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label_ketthuc;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label_diemdanh;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label_sdt;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label_ngay;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label_trangthai;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}