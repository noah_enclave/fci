﻿namespace FCI.View
{
    partial class Tracking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Tracking));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label_tiethoc = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label_vangtong = new System.Windows.Forms.Label();
            this.label_muonhoc = new System.Windows.Forms.Label();
            this.label_svdk = new System.Windows.Forms.Label();
            this.label_khkt = new System.Windows.Forms.Label();
            this.label_khbd = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.dataGridView_liststudent = new System.Windows.Forms.DataGridView();
            this.Column_idStudent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_S_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_class = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_today = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox_listcourse = new System.Windows.Forms.ComboBox();
            this.comboBox_schoolyear = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox_gio = new System.Windows.Forms.TextBox();
            this.textBox_thungay = new System.Windows.Forms.TextBox();
            this.textBox_tiethoc = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label_vang = new System.Windows.Forms.Label();
            this.label_tre = new System.Windows.Forms.Label();
            this.label_hoten = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox_hinhanh = new System.Windows.Forms.PictureBox();
            this.listView_chitiet = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_opencamera = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_opencalendar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripDropDownButton3 = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.đIỂMDANHToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_dunggio = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_dimuon = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_vanghoc = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_liststudent)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_hinhanh)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.statusStrip1);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1088, 549);
            this.panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel3.Controls.Add(this.splitContainer1);
            this.panel3.Controls.Add(this.toolStrip1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1088, 527);
            this.panel3.TabIndex = 40;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Location = new System.Drawing.Point(0, 45);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox5);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.splitContainer1.Panel2.Controls.Add(this.groupBox1);
            this.splitContainer1.Size = new System.Drawing.Size(1088, 479);
            this.splitContainer1.SplitterDistance = 533;
            this.splitContainer1.TabIndex = 39;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox5.Controls.Add(this.panel4);
            this.groupBox5.Controls.Add(this.dataGridView_liststudent);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.comboBox_listcourse);
            this.groupBox5.Controls.Add(this.comboBox_schoolyear);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(0, 0);
            this.groupBox5.MinimumSize = new System.Drawing.Size(462, 200);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(531, 477);
            this.groupBox5.TabIndex = 37;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Danh sách lớp";
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.Controls.Add(this.label_tiethoc);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.label_vangtong);
            this.panel4.Controls.Add(this.label_muonhoc);
            this.panel4.Controls.Add(this.label_svdk);
            this.panel4.Controls.Add(this.label_khkt);
            this.panel4.Controls.Add(this.label_khbd);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.panel4.Location = new System.Drawing.Point(5, 397);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(520, 79);
            this.panel4.TabIndex = 5;
            // 
            // label_tiethoc
            // 
            this.label_tiethoc.AutoSize = true;
            this.label_tiethoc.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label_tiethoc.Location = new System.Drawing.Point(266, 5);
            this.label_tiethoc.Name = "label_tiethoc";
            this.label_tiethoc.Size = new System.Drawing.Size(65, 15);
            this.label_tiethoc.TabIndex = 0;
            this.label_tiethoc.Text = "Tiết học: _";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label6.Location = new System.Drawing.Point(266, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(123, 15);
            this.label6.TabIndex = 0;
            this.label6.Text = "Số SV vắng / tổng SV:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label7.Location = new System.Drawing.Point(266, 54);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(133, 15);
            this.label7.TabIndex = 0;
            this.label7.Text = "Số SV đi muộn / đi học:";
            // 
            // label_vangtong
            // 
            this.label_vangtong.AutoSize = true;
            this.label_vangtong.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label_vangtong.Location = new System.Drawing.Point(407, 29);
            this.label_vangtong.Name = "label_vangtong";
            this.label_vangtong.Size = new System.Drawing.Size(14, 15);
            this.label_vangtong.TabIndex = 0;
            this.label_vangtong.Text = "_";
            // 
            // label_muonhoc
            // 
            this.label_muonhoc.AutoSize = true;
            this.label_muonhoc.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label_muonhoc.Location = new System.Drawing.Point(407, 54);
            this.label_muonhoc.Name = "label_muonhoc";
            this.label_muonhoc.Size = new System.Drawing.Size(14, 15);
            this.label_muonhoc.TabIndex = 0;
            this.label_muonhoc.Text = "_";
            // 
            // label_svdk
            // 
            this.label_svdk.AutoSize = true;
            this.label_svdk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label_svdk.Location = new System.Drawing.Point(124, 54);
            this.label_svdk.Name = "label_svdk";
            this.label_svdk.Size = new System.Drawing.Size(14, 15);
            this.label_svdk.TabIndex = 0;
            this.label_svdk.Text = "_";
            // 
            // label_khkt
            // 
            this.label_khkt.AutoSize = true;
            this.label_khkt.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label_khkt.Location = new System.Drawing.Point(124, 29);
            this.label_khkt.Name = "label_khkt";
            this.label_khkt.Size = new System.Drawing.Size(14, 15);
            this.label_khkt.TabIndex = 0;
            this.label_khkt.Text = "_";
            // 
            // label_khbd
            // 
            this.label_khbd.AutoSize = true;
            this.label_khbd.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label_khbd.Location = new System.Drawing.Point(124, 5);
            this.label_khbd.Name = "label_khbd";
            this.label_khbd.Size = new System.Drawing.Size(14, 15);
            this.label_khbd.TabIndex = 0;
            this.label_khbd.Text = "_";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label14.Location = new System.Drawing.Point(9, 54);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(105, 15);
            this.label14.TabIndex = 0;
            this.label14.Text = "Sinh viên đăng ký:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label12.Location = new System.Drawing.Point(7, 29);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(107, 15);
            this.label12.TabIndex = 0;
            this.label12.Text = "Khóa học kết thúc:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label11.Location = new System.Drawing.Point(7, 5);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(106, 15);
            this.label11.TabIndex = 0;
            this.label11.Text = "Khóa học bắt đầu:";
            // 
            // dataGridView_liststudent
            // 
            this.dataGridView_liststudent.AllowUserToAddRows = false;
            this.dataGridView_liststudent.AllowUserToDeleteRows = false;
            this.dataGridView_liststudent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_liststudent.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView_liststudent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_liststudent.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column_idStudent,
            this.Column_S_Name,
            this.Column_class,
            this.Column_today});
            this.dataGridView_liststudent.Location = new System.Drawing.Point(6, 49);
            this.dataGridView_liststudent.MultiSelect = false;
            this.dataGridView_liststudent.Name = "dataGridView_liststudent";
            this.dataGridView_liststudent.RowHeadersVisible = false;
            this.dataGridView_liststudent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView_liststudent.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_liststudent.Size = new System.Drawing.Size(519, 341);
            this.dataGridView_liststudent.TabIndex = 0;
            this.dataGridView_liststudent.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dataGridView_liststudent_EditingControlShowing);
            this.dataGridView_liststudent.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_liststudent_RowEnter);
            // 
            // Column_idStudent
            // 
            this.Column_idStudent.DataPropertyName = "idStudent";
            this.Column_idStudent.HeaderText = "ID";
            this.Column_idStudent.Name = "Column_idStudent";
            this.Column_idStudent.ReadOnly = true;
            // 
            // Column_S_Name
            // 
            this.Column_S_Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column_S_Name.DataPropertyName = "S_Name";
            this.Column_S_Name.FillWeight = 150F;
            this.Column_S_Name.HeaderText = "Họ Tên";
            this.Column_S_Name.Name = "Column_S_Name";
            this.Column_S_Name.ReadOnly = true;
            // 
            // Column_class
            // 
            this.Column_class.DataPropertyName = "idClass";
            this.Column_class.HeaderText = "Lớp sinh hoạt";
            this.Column_class.Name = "Column_class";
            this.Column_class.ReadOnly = true;
            this.Column_class.Width = 140;
            // 
            // Column_today
            // 
            this.Column_today.DataPropertyName = "diemdanh";
            this.Column_today.DisplayStyleForCurrentCellOnly = true;
            this.Column_today.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Column_today.HeaderText = "Tiết học: _";
            this.Column_today.Name = "Column_today";
            this.Column_today.Width = 120;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label1.Location = new System.Drawing.Point(210, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Lớp tín chỉ:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label5.Location = new System.Drawing.Point(14, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 15);
            this.label5.TabIndex = 3;
            this.label5.Text = "Năm học: ";
            // 
            // comboBox_listcourse
            // 
            this.comboBox_listcourse.DisplayMember = "CourseName";
            this.comboBox_listcourse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_listcourse.FormattingEnabled = true;
            this.comboBox_listcourse.Location = new System.Drawing.Point(283, 18);
            this.comboBox_listcourse.Name = "comboBox_listcourse";
            this.comboBox_listcourse.Size = new System.Drawing.Size(222, 23);
            this.comboBox_listcourse.TabIndex = 2;
            this.comboBox_listcourse.ValueMember = "idCourse";
            this.comboBox_listcourse.SelectedIndexChanged += new System.EventHandler(this.comboBox_listcourse_SelectedIndexChanged);
            // 
            // comboBox_schoolyear
            // 
            this.comboBox_schoolyear.DisplayMember = "CourseName";
            this.comboBox_schoolyear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_schoolyear.FormattingEnabled = true;
            this.comboBox_schoolyear.Location = new System.Drawing.Point(81, 18);
            this.comboBox_schoolyear.Name = "comboBox_schoolyear";
            this.comboBox_schoolyear.Size = new System.Drawing.Size(119, 23);
            this.comboBox_schoolyear.TabIndex = 1;
            this.comboBox_schoolyear.ValueMember = "idCourse";
            this.comboBox_schoolyear.SelectedIndexChanged += new System.EventHandler(this.comboBox_schoolyear_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Controls.Add(this.textBox_gio);
            this.groupBox1.Controls.Add(this.textBox_thungay);
            this.groupBox1.Controls.Add(this.textBox_tiethoc);
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.pictureBox_hinhanh);
            this.groupBox1.Controls.Add(this.listView_chitiet);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.MinimumSize = new System.Drawing.Size(421, 200);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(549, 477);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Chi tiết";
            // 
            // textBox_gio
            // 
            this.textBox_gio.Location = new System.Drawing.Point(143, 135);
            this.textBox_gio.Name = "textBox_gio";
            this.textBox_gio.ReadOnly = true;
            this.textBox_gio.Size = new System.Drawing.Size(131, 21);
            this.textBox_gio.TabIndex = 3;
            this.textBox_gio.Text = "Thời Gian: _";
            this.textBox_gio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_thungay
            // 
            this.textBox_thungay.Location = new System.Drawing.Point(280, 135);
            this.textBox_thungay.Name = "textBox_thungay";
            this.textBox_thungay.ReadOnly = true;
            this.textBox_thungay.Size = new System.Drawing.Size(148, 21);
            this.textBox_thungay.TabIndex = 3;
            this.textBox_thungay.Text = "Thứ 3, 01/12/2017";
            this.textBox_thungay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_tiethoc
            // 
            this.textBox_tiethoc.Location = new System.Drawing.Point(6, 135);
            this.textBox_tiethoc.Name = "textBox_tiethoc";
            this.textBox_tiethoc.ReadOnly = true;
            this.textBox_tiethoc.Size = new System.Drawing.Size(131, 21);
            this.textBox_tiethoc.TabIndex = 3;
            this.textBox_tiethoc.Text = "Tiết học: _";
            this.textBox_tiethoc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label_vang);
            this.panel2.Controls.Add(this.label_tre);
            this.panel2.Controls.Add(this.label_hoten);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.panel2.Location = new System.Drawing.Point(131, 21);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(412, 108);
            this.panel2.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label4.Location = new System.Drawing.Point(4, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(130, 15);
            this.label4.TabIndex = 0;
            this.label4.Text = "Số buổi vắng / hiện tại:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label3.Location = new System.Drawing.Point(4, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(142, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "Số buổi đi muộn / đi học:";
            // 
            // label_vang
            // 
            this.label_vang.AutoSize = true;
            this.label_vang.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label_vang.Location = new System.Drawing.Point(163, 48);
            this.label_vang.Name = "label_vang";
            this.label_vang.Size = new System.Drawing.Size(14, 15);
            this.label_vang.TabIndex = 0;
            this.label_vang.Text = "_";
            // 
            // label_tre
            // 
            this.label_tre.AutoSize = true;
            this.label_tre.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label_tre.Location = new System.Drawing.Point(163, 78);
            this.label_tre.Name = "label_tre";
            this.label_tre.Size = new System.Drawing.Size(14, 15);
            this.label_tre.TabIndex = 0;
            this.label_tre.Text = "_";
            // 
            // label_hoten
            // 
            this.label_hoten.AutoSize = true;
            this.label_hoten.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label_hoten.Location = new System.Drawing.Point(163, 18);
            this.label_hoten.Name = "label_hoten";
            this.label_hoten.Size = new System.Drawing.Size(14, 15);
            this.label_hoten.TabIndex = 0;
            this.label_hoten.Text = "_";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label2.Location = new System.Drawing.Point(4, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "Họ Tên Sinh Viên:";
            // 
            // pictureBox_hinhanh
            // 
            this.pictureBox_hinhanh.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pictureBox_hinhanh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox_hinhanh.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_hinhanh.Location = new System.Drawing.Point(6, 21);
            this.pictureBox_hinhanh.Name = "pictureBox_hinhanh";
            this.pictureBox_hinhanh.Size = new System.Drawing.Size(113, 108);
            this.pictureBox_hinhanh.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_hinhanh.TabIndex = 1;
            this.pictureBox_hinhanh.TabStop = false;
            // 
            // listView_chitiet
            // 
            this.listView_chitiet.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.listView_chitiet.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView_chitiet.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader4,
            this.columnHeader6,
            this.columnHeader5});
            this.listView_chitiet.FullRowSelect = true;
            this.listView_chitiet.HideSelection = false;
            this.listView_chitiet.Location = new System.Drawing.Point(6, 162);
            this.listView_chitiet.Name = "listView_chitiet";
            this.listView_chitiet.Size = new System.Drawing.Size(537, 309);
            this.listView_chitiet.TabIndex = 4;
            this.listView_chitiet.UseCompatibleStateImageBehavior = false;
            this.listView_chitiet.View = System.Windows.Forms.View.Details;
            this.listView_chitiet.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listView_chitiet_MouseClick);
            this.listView_chitiet.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listView_chitiet_MouseDoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Tiết";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Buổi";
            this.columnHeader2.Width = 81;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Giờ Bắt đầu";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader4.Width = 90;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Giờ Điểm danh";
            this.columnHeader6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader6.Width = 109;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Giờ Kết thúc";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader5.Width = 90;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.toolStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.toolStrip1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(30, 30);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator4,
            this.toolStripButton_opencamera,
            this.toolStripSeparator5,
            this.toolStripButton_opencalendar,
            this.toolStripSeparator1,
            this.toolStripLabel1,
            this.toolStripTextBox1,
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripSeparator2,
            this.toolStripDropDownButton3,
            this.toolStripSeparator3,
            this.toolStripDropDownButton2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(1088, 43);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.AutoSize = false;
            this.toolStripSeparator4.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.toolStripSeparator4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 40);
            // 
            // toolStripButton_opencamera
            // 
            this.toolStripButton_opencamera.AutoSize = false;
            this.toolStripButton_opencamera.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.toolStripButton_opencamera.Image = global::FCI.Properties.Resources.camera;
            this.toolStripButton_opencamera.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_opencamera.Name = "toolStripButton_opencamera";
            this.toolStripButton_opencamera.Size = new System.Drawing.Size(103, 40);
            this.toolStripButton_opencamera.Text = "Mở Camera";
            this.toolStripButton_opencamera.ToolTipText = "Điểm danh bằng Camera";
            this.toolStripButton_opencamera.Click += new System.EventHandler(this.toolStripButton_opencamera_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.AutoSize = false;
            this.toolStripSeparator5.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.toolStripSeparator5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 40);
            // 
            // toolStripButton_opencalendar
            // 
            this.toolStripButton_opencalendar.AutoSize = false;
            this.toolStripButton_opencalendar.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.toolStripButton_opencalendar.Image = global::FCI.Properties.Resources.calendar;
            this.toolStripButton_opencalendar.ImageTransparentColor = System.Drawing.Color.MediumAquamarine;
            this.toolStripButton_opencalendar.Name = "toolStripButton_opencalendar";
            this.toolStripButton_opencalendar.Size = new System.Drawing.Size(139, 40);
            this.toolStripButton_opencalendar.Text = "Hiển thị lịch tháng";
            this.toolStripButton_opencalendar.Visible = false;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.AutoSize = false;
            this.toolStripSeparator1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.toolStripSeparator1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 40);
            this.toolStripSeparator1.Visible = false;
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.AutoSize = false;
            this.toolStripLabel1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(112, 40);
            this.toolStripLabel1.Text = "Tìm kiếm Sinh Viên:";
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.AutoSize = false;
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(200, 40);
            this.toolStripTextBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.toolStripTextBox1_KeyDown);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.AutoSize = false;
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(22, 22);
            this.toolStripButton1.Text = "X";
            this.toolStripButton1.ToolTipText = "Hủy tìm kiếm";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.AutoSize = false;
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.toolStripButton2.Image = global::FCI.Properties.Resources.searchbutton;
            this.toolStripButton2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(24, 24);
            this.toolStripButton2.Text = "Tìm";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.AutoSize = false;
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 40);
            // 
            // toolStripDropDownButton3
            // 
            this.toolStripDropDownButton3.AutoSize = false;
            this.toolStripDropDownButton3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3,
            this.toolStripMenuItem4});
            this.toolStripDropDownButton3.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.toolStripDropDownButton3.Image = global::FCI.Properties.Resources.setting;
            this.toolStripDropDownButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton3.Name = "toolStripDropDownButton3";
            this.toolStripDropDownButton3.Size = new System.Drawing.Size(155, 40);
            this.toolStripDropDownButton3.Text = "Tùy chỉnh hiển thị";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.AutoSize = false;
            this.toolStripMenuItem3.AutoToolTip = true;
            this.toolStripMenuItem3.Checked = true;
            this.toolStripMenuItem3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripMenuItem3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(161, 22);
            this.toolStripMenuItem3.Text = "Toàn thời gian";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(161, 22);
            this.toolStripMenuItem4.Text = "Cho đến bây giờ";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.AutoSize = false;
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 40);
            // 
            // toolStripDropDownButton2
            // 
            this.toolStripDropDownButton2.AutoSize = false;
            this.toolStripDropDownButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2});
            this.toolStripDropDownButton2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.toolStripDropDownButton2.Image = global::FCI.Properties.Resources.excel;
            this.toolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
            this.toolStripDropDownButton2.Size = new System.Drawing.Size(113, 40);
            this.toolStripDropDownButton2.Text = "Xuất dữ liệu";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(123, 22);
            this.toolStripMenuItem1.Text = "Sinh Viên";
            this.toolStripMenuItem1.Visible = false;
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(123, 22);
            this.toolStripMenuItem2.Text = "Toàn Lớp";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel3,
            this.toolStripStatusLabel4});
            this.statusStrip1.Location = new System.Drawing.Point(0, 527);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1088, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(62, 17);
            this.toolStripStatusLabel1.Text = "Chú thích:";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.BackColor = System.Drawing.Color.LightGreen;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(124, 17);
            this.toolStripStatusLabel2.Text = "[Điểm danh đúng giờ]";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.BackColor = System.Drawing.Color.LightSkyBlue;
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(108, 17);
            this.toolStripStatusLabel3.Text = "[Điểm danh muộn]";
            // 
            // toolStripStatusLabel4
            // 
            this.toolStripStatusLabel4.BackColor = System.Drawing.Color.LightGray;
            this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            this.toolStripStatusLabel4.Size = new System.Drawing.Size(110, 17);
            this.toolStripStatusLabel4.Text = "[Không điểm danh]";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 5000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.đIỂMDANHToolStripMenuItem,
            this.toolStripMenuItem_dunggio,
            this.toolStripMenuItem_dimuon,
            this.toolStripMenuItem_vanghoc});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(140, 92);
            // 
            // đIỂMDANHToolStripMenuItem
            // 
            this.đIỂMDANHToolStripMenuItem.Enabled = false;
            this.đIỂMDANHToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.đIỂMDANHToolStripMenuItem.Name = "đIỂMDANHToolStripMenuItem";
            this.đIỂMDANHToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.đIỂMDANHToolStripMenuItem.Text = "ĐIỂM DANH";
            this.đIỂMDANHToolStripMenuItem.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            // 
            // toolStripMenuItem_dunggio
            // 
            this.toolStripMenuItem_dunggio.BackColor = System.Drawing.Color.LightGreen;
            this.toolStripMenuItem_dunggio.ForeColor = System.Drawing.Color.Black;
            this.toolStripMenuItem_dunggio.Name = "toolStripMenuItem_dunggio";
            this.toolStripMenuItem_dunggio.Size = new System.Drawing.Size(139, 22);
            this.toolStripMenuItem_dunggio.Tag = "0";
            this.toolStripMenuItem_dunggio.Text = "Đúng &giờ";
            this.toolStripMenuItem_dunggio.Click += new System.EventHandler(this.toolStripMenuItem__Click_1);
            // 
            // toolStripMenuItem_dimuon
            // 
            this.toolStripMenuItem_dimuon.BackColor = System.Drawing.Color.LightSkyBlue;
            this.toolStripMenuItem_dimuon.ForeColor = System.Drawing.Color.Black;
            this.toolStripMenuItem_dimuon.Name = "toolStripMenuItem_dimuon";
            this.toolStripMenuItem_dimuon.Size = new System.Drawing.Size(139, 22);
            this.toolStripMenuItem_dimuon.Tag = "1";
            this.toolStripMenuItem_dimuon.Text = "Đi &muộn";
            this.toolStripMenuItem_dimuon.Click += new System.EventHandler(this.toolStripMenuItem__Click_1);
            // 
            // toolStripMenuItem_vanghoc
            // 
            this.toolStripMenuItem_vanghoc.BackColor = System.Drawing.Color.LightGray;
            this.toolStripMenuItem_vanghoc.ForeColor = System.Drawing.Color.Black;
            this.toolStripMenuItem_vanghoc.Name = "toolStripMenuItem_vanghoc";
            this.toolStripMenuItem_vanghoc.Size = new System.Drawing.Size(139, 22);
            this.toolStripMenuItem_vanghoc.Tag = "2";
            this.toolStripMenuItem_vanghoc.Text = "Vắng &học";
            this.toolStripMenuItem_vanghoc.Click += new System.EventHandler(this.toolStripMenuItem__Click_1);
            // 
            // Tracking
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1112, 573);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Tracking";
            this.Text = "Điểm danh";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Tracking_FormClosed);
            this.Load += new System.EventHandler(this.Tracking_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_liststudent)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_hinhanh)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        //private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataGridView dataGridView_liststudent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBox_listcourse;
        private System.Windows.Forms.ComboBox comboBox_schoolyear;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox_gio;
        private System.Windows.Forms.TextBox textBox_thungay;
        private System.Windows.Forms.TextBox textBox_tiethoc;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label_vang;
        private System.Windows.Forms.Label label_tre;
        private System.Windows.Forms.Label label_hoten;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox_hinhanh;
        private System.Windows.Forms.ListView listView_chitiet;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton_opencamera;
        private System.Windows.Forms.ToolStripButton toolStripButton_opencalendar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem đIỂMDANHToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_dunggio;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_dimuon;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_vanghoc;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label_vangtong;
        private System.Windows.Forms.Label label_muonhoc;
        private System.Windows.Forms.Label label_svdk;
        private System.Windows.Forms.Label label_khkt;
        private System.Windows.Forms.Label label_khbd;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label_tiethoc;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_idStudent;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_S_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_class;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column_today;
    }
}