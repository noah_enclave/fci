﻿using FCI.Struct;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FCI.View
{
    public partial class TrackingDetailLession : Form
    {
        private LessonDetail obj;
        public Dictionary<Color, string> ARRAY_COLOR = new Dictionary<Color, string>();
        public TrackingDetailLession(object obj = null)
        {
            InitializeComponent();
            this.obj = obj as LessonDetail;
            ARRAY_COLOR.Add(Color.LightGreen, "Đúng giờ");
            ARRAY_COLOR.Add(Color.LightSkyBlue, "Đi muộn");
            ARRAY_COLOR.Add(Color.LightGray, "Vắng học");
        }

        private void TrackingDetailLession_Load(object sender, EventArgs e)
        {
            pictureBox_avatar.Image = Func.F.byteArray2Image(obj.student.S_Avatar) ?? Properties.Resources.no_avatar;
            label_hoten.Text = obj.student.S_Name;
            label_sdt.Text = obj.student.S_Phone;
            label_vangtong.Text = obj.vangtong;
            label_muonhoc.Text = obj.muonhoc;

            label_tietthu.Text = obj.lesson.index + "";
            label_ngay.Text = obj.lesson.date.ToString();
            label_batdau.Text = obj.lesson.time.getLessonTimeStart();
            label_ketthuc.Text = obj.lesson.time.getLessonTimeEnd();
            label_diemdanh.Text = obj.giodiemdanh;
            pictureBox_hinhanh.Image = obj.image ?? Properties.Resources.notepad;

            label_trangthai.BackColor = obj.item.BackColor;
            label_trangthai.Text = ARRAY_COLOR[obj.item.BackColor];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (pictureBox_hinhanh.Image == null) return;
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Filter = "Hình ảnh|*.jpg";
                sfd.FileName = obj.lesson.index + "_" + obj.student.idStudent + ".jpg";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    pictureBox_hinhanh.Image.Save(sfd.FileName, ImageFormat.Jpeg);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}
