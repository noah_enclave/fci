﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using FCI.Struct;
using FCI.Func;
using System.Threading;
using FCI.Data;
using System.Data;
using System.ComponentModel;
using System.Drawing;

namespace FCI.View
{
    public partial class Tracking : Form
    {
        #region Variables
        private IEnumerable<Data.Course> courseList;
        public IEnumerable<Data.Student> studentList;
        private int tietHienTai, tietTong, tietTre, tietVang;
        private List<Lesson> lessonList = new List<Lesson>();
        private IEnumerable<Data.Tracking> lastLogStudent = null;
        private Student lastStudent = null;
        private STUDENT_COURSE lastSC = null;
        private Course lastCourse = null;
        private bool loadfull = true;
        //private Tracking_Camera tracking_camera = null;
        public Color[] ARRAY_COLOR = { Color.LightGreen, Color.LightSkyBlue, Color.LightGray, Color.Transparent };
        public string[] ARRAY_NAME = new string[] { "Đúng giờ", "Đi muộn", "Vắng học" };
        #endregion

        public Tracking()
        {
            InitializeComponent();
        }

        private void Tracking_Load(object sender, EventArgs e)
        {
            textBox_gio.Text = string.Format("Thời Gian: {0}", DateTime.Now.ToString("HH:mm"));
            textBox_thungay.Text = (new LessonDate(DateTime.Now)).ToString();
            // Setting data grid
            dataGridView_liststudent.AutoGenerateColumns = false;
            dataGridView_liststudent.AllowUserToResizeRows = false;
            //dataGridView_liststudent.RowTemplate.Height = 30;
            //Column_today.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            Column_today.Items.AddRange(ARRAY_NAME);
            // Setup data
            loadYear();
        }

        private void loadYear()
        {
            comboBox_schoolyear.Items.AddRange(F.yearSchool());
            var t = DateTime.Now.Month;
            var n = DateTime.Now.Year;
            string sy = "";
            if (t < 8) sy = (n - 1) + "-" + n;
            else sy = n + "-" + (n + 1);
            comboBox_schoolyear.SelectedItem = sy;
        }

        #region Function calling

        private void init_comboboxCourse()
        {
            //
            comboBox_listcourse.DataSource = courseList = (from cou in Program.db.Course
                                                           join ins in Program.db.Instructor on cou.idInstructor equals ins.idInstructor
                                                           join acc in Program.db.Account on ins.I_idAccount equals acc.idAccount
                                                           where acc.idAccount == Data.Session.idAccount && cou.SchoolYear == comboBox_schoolyear.Text
                                                           select cou).ToList();
        }
        private void init_gridviewStudent()
        {
            dataGridView_liststudent.DataSource = studentList = (from stu in Program.db.Student
                                                                 join sc in Program.db.STUDENT_COURSE on stu.idStudent equals sc.idStudent
                                                                 join cou in Program.db.Course on sc.idCourse equals cou.idCourse
                                                                 where cou.idCourse == comboBox_listcourse.SelectedValue.ToString() && sc.SchoolYear == cou.SchoolYear
                                                                 select stu).ToList();

        }
        private void init_detail(Data.Student sv, bool rerender = false)
        {
            label_hoten.Text = sv.S_Name;
            pictureBox_hinhanh.Image = (lastStudent.S_Avatar == null) ? Properties.Resources.no_avatar : F.byteArray2Image(lastStudent.S_Avatar);
            lastSC = sv.STUDENT_COURSE.SingleOrDefault(x =>
                x.idCourse == comboBox_listcourse.SelectedValue.ToString() &&
                x.SchoolYear == comboBox_schoolyear.Text);
            init_lessonList(sv);
        }

        private void load_course(string idCourse)
        {
            lastCourse = (from cou in courseList where cou.idCourse == idCourse select cou).FirstOrDefault();
            var sch = Program.db.Schedule.FirstOrDefault(sc => sc.idCourse == idCourse && sc.SchoolYear == comboBox_schoolyear.Text);
            if (sch == null)
            {
                MessageBox.Show("Lớp tín chỉ chưa có lịch");
                return;
            }
            string[] dow = { sch.Sun, sch.Mon, sch.Tue, sch.Wed, sch.Thu, sch.Fri, sch.Sat, };
            lessonList.Clear();

            // init for student

            int lessonCount = 0;
            string[][] lessonInDayOfWeek = new string[7][];
            for (int i = 0; i < 7; i++) lessonInDayOfWeek[i] = new string[5];


            tietHienTai = 0;
            var now = DateTime.Now.AddMinutes(US.minEarly);

            foreach (DateTime day in F.EachDay(lastCourse.StartDate, lastCourse.EndDate))
            {
                var i = (int)day.DayOfWeek;
                if (lessonInDayOfWeek[i].Length == 5)
                {
                    if (dow[i] == "" || dow[i] == null) continue;
                    lessonInDayOfWeek[i] = dow[i].Split(';');
                }
                foreach (string lessonTime in lessonInDayOfWeek[i])
                {
                    if (lessonTime == "") continue;
                    lessonList.Add(new Lesson(++lessonCount, day, lessonTime));

                    if (DateTime.Compare(now, lessonList.Last().fullTimeStart) >= 0)
                    {
                        ++tietHienTai;
                    }
                }
            }
            tietTong = lessonList.Count;
            init_gridviewStudent();

            // print
            textBox_tiethoc.Text = Column_today.HeaderText = label_tiethoc.Text = string.Format("Tiết học: {0} / {1}", tietHienTai, tietTong);
            label_khbd.Text = (new LessonDate(lastCourse.StartDate)).ToString();
            label_khkt.Text = (new LessonDate(lastCourse.EndDate)).ToString();
            label_svdk.Text = dataGridView_liststudent.Rows.Count.ToString();
            Console.WriteLine(tietHienTai);
            Column_today.Visible = tietHienTai != 0;
            if (Column_today.Visible) init_combobox();
        }

        private void init_combobox()
        {
            int index = 0;
            int count = 0;
            foreach (DataGridViewRow row in dataGridView_liststudent.Rows)
            {
                var sc = studentList.ElementAt(count).STUDENT_COURSE.SingleOrDefault(x =>
                    x.idCourse == comboBox_listcourse.SelectedValue.ToString() &&
                    x.SchoolYear == comboBox_schoolyear.Text);

                var log = Program.db.Tracking.FirstOrDefault(x => x.No == tietHienTai && x.idSC == sc.idSC);
                if (log == null) index = 2;
                else if (TimeSpan.Compare(log.Logs.TimeOfDay, lessonList[tietHienTai - 1].fullTimeStart.AddMinutes(US.minLate).TimeOfDay) > 0) index = 1;
                else index = 0;

                var cel = row.Cells[3] as DataGridViewComboBoxCell;
                cel.Value = cel.Items[index];
                cel.Style.BackColor = ARRAY_COLOR[index];
                ++count;
            }
            setSVCount();
        }

        private void init_lessonList(Data.Student sv, bool render = true)
        {
            if (lessonList.Count == 0) return;
            var idCourse = comboBox_listcourse.SelectedValue.ToString();
            if (sv != null)
            {
                lastLogStudent = (from sc in Program.db.STUDENT_COURSE
                                  join tra in Program.db.Tracking on sc.idSC equals tra.idSC
                                  where sc.idStudent == sv.idStudent &&
                                        sc.idCourse == idCourse &&
                                        sc.SchoolYear == comboBox_schoolyear.Text
                                  select tra).ToList();
            }
            init_listView(lessonList, lastLogStudent, loadfull, render);
        }

        private int timeCompare(string f, string l)
        {
            DateTime t1 = DateTime.Parse("2012/12/12 " + f + ":00.000");
            DateTime t2 = DateTime.Parse("2012/12/12 " + l + ":00.000");
            //Console.WriteLine(
            //    t1.ToString() + " @ " + t2.ToString() 
            //    + " \t " + TimeSpan.Compare(t1.TimeOfDay, t2.TimeOfDay)
            //    + " \t " + TimeSpan.Compare(t1.TimeOfDay, t2.AddMinutes(phutDiMuon).TimeOfDay)
            //    );
            return TimeSpan.Compare(t1.TimeOfDay, t2.AddMinutes(US.minLate).TimeOfDay);
        }

        private void init_listView(List<Lesson> lessonList, IEnumerable<Data.Tracking> logStudent, bool loadfull = true, bool render = true)
        {
            if (render) listView_chitiet.Groups.Clear();
            if (render) listView_chitiet.Items.Clear();
            if (lessonList.Count == 0) return;
            ListViewGroup lastListViewGroup = new ListViewGroup(lessonList[0].ToString());
            int listLength = lessonList.Count;

            // tietTong = listLength;
            // tietHienTai = 0;
            tietVang = 0;
            tietTre = 0;
            int count = 0;
            var now = DateTime.Now.AddMinutes(US.minEarly);
            for (var i = 0; i < listLength; i++)
            {
                if (render)
                    if (lastListViewGroup.Header != lessonList[i].ToString())
                        lastListViewGroup = new ListViewGroup(lessonList[i].ToString());
                var sl = logStudent.Where(ls => ls.No == (i + 1)).FirstOrDefault();
                if (sl != null && DateTime.Compare(sl.Logs, lessonList[i].fullTimeEnd) <= 0) // ontime
                {
                    var elm = lessonList[i].ToArray();
                    elm[3] = sl.Logs.ToString("HH:mm");
                    if (render) listView_chitiet.Items.Add(new ListViewItem(elm, lastListViewGroup));
                    if (timeCompare(elm[3], elm[2]) > 0)
                    {
                        ++tietTre;
                        listView_chitiet.Items[i].BackColor = ARRAY_COLOR[1];

                    }
                    else
                    {
                        listView_chitiet.Items[i].BackColor = ARRAY_COLOR[0];
                    }
                }
                else
                {
                    if (render) listView_chitiet.Items.Add(new ListViewItem(lessonList[i].ToArray(), lastListViewGroup));
                    if (DateTime.Compare(now, lessonList[i].fullTimeStart) >= 0)
                    {
                        listView_chitiet.Items[i].BackColor = ARRAY_COLOR[2];
                    }
                    else if (!loadfull && render)
                    {
                        ++count;
                    }
                }

                if (render) listView_chitiet.Groups.Add(lastListViewGroup);
            }
            if (!loadfull)
            {
                for (int i = listView_chitiet.Items.Count - 1; i >= 0 && count > 0; i--, count--)
                {
                    listView_chitiet.Items.RemoveAt(i);
                }
            }
            if (tietHienTai > 0) listView_chitiet.EnsureVisible(tietHienTai - 1);
            tietVang = tietHienTai - logStudent.Count();
            setLessionCount();
            listView_chitiet.Refresh();
            setSVCount();
        }


        private void setLessionCount()
        {
            if (tietHienTai == 0) return;
            lastSC.NoAbsent = tietVang;
            lastSC.NoLate = tietTre;
            label_vang.Text = tietVang + " / " + tietHienTai;
            label_tre.Text = tietTre + " / " + (tietHienTai - tietVang);
            Program.db.SaveChanges();
        }

        private void setSVCount()
        {
            if (tietHienTai != 0)
            {
                int vang = 0, muon = 0, tong = 0;
                foreach (DataGridViewRow row in dataGridView_liststudent.Rows)
                {
                    switch (getIndexColor(row.Cells[3].Value.ToString()))
                    {
                        case 1:
                            ++muon;
                            break;
                        case 2:
                            ++vang;
                            break;
                    }
                }
                if (int.TryParse(label_svdk.Text, out tong))
                {
                    label_vangtong.Text = vang + " / " + tong;
                    label_muonhoc.Text = muon + " / " + (tong - vang);
                }
            }
        }

        // Extend func

        #endregion

        private void refreshVariables()
        {
            tietHienTai = tietTong = tietTre = tietVang = 0;
            lessonList = new List<Lesson>();
            lastLogStudent = null;
            lastStudent = null;
            lastSC = null;
            lastCourse = null;
            toolStripTextBox1.Text = "";
            loadfull = true;
            toolStripMenuItem3.Checked = loadfull;
            toolStripMenuItem4.Checked = !loadfull;
            dataGridView_liststudent.DataSource = null;
            listView_chitiet.Groups.Clear();
            listView_chitiet.Items.Clear();

            textBox_tiethoc.Text = label_tiethoc.Text = "Tiết học: _";
            label_khbd.Text = label_khkt.Text = label_svdk.Text = label_vangtong.Text = label_muonhoc.Text = "_";
            label_hoten.Text = label_vang.Text = label_tre.Text = "_";
            pictureBox_hinhanh.Image = null;
        }

        private void comboBox_schoolyear_SelectedIndexChanged(object sender, EventArgs e)
        {
            refreshVariables();
            init_comboboxCourse();
        }

        private void toolStripButton_opencamera_Click(object sender, EventArgs e)
        {
            if (Program.grabber != null)
            {
                MessageBox.Show("Camera chưa sẵn sàng!");
                return;
            }
            if (TrackingCamera.opening)
            {
                Program.trackingcamera.Activate();
                return;
            }
            (Program.trackingcamera = new TrackingCamera(comboBox_schoolyear.SelectedIndex, comboBox_listcourse.SelectedIndex)).Show();
        }

        private void comboBox_listcourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox_listcourse.SelectedIndex < 0) return;
            refreshVariables();
            load_course(comboBox_listcourse.SelectedValue.ToString());

            // tuy thuoc vao muc tuy chinh
        }

        private void toolStripTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                toolStripButton2_Click(null, null);
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            toolStripTextBox1.Text = "";
            dataGridView_liststudent.DataSource = studentList;
            refeshFinder();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            var value = toolStripTextBox1.Text.ToLower().Trim();
            if (value == "")
            {
                dataGridView_liststudent.DataSource = studentList;
            }
            else
            {
                if (dataGridView_liststudent.DataSource == null || dataGridView_liststudent.Rows.Count == 0) return;
                dataGridView_liststudent.DataSource = (from stu in studentList
                                                       join cla in Program.db.Class on stu.idClass equals cla.idClass
                                                       where (stu.S_Name.ToLower().Contains(value) ||
                                                             stu.idStudent.ToLower().Contains(value) ||
                                                             cla.idClass.ToLower().Contains(value) ||
                                                             stu.diemdanh.ToLower().Contains(value))
                                                       select stu).ToList();

            }

            refeshFinder();
        }

        private void refeshFinder()
        {
            foreach (DataGridViewRow row in dataGridView_liststudent.Rows)
            {
                var cel = row.Cells[3] as DataGridViewComboBoxCell;
                cel.Style.BackColor = ARRAY_COLOR[getIndexColor(cel.Value.ToString())];
            }
            dataGridView_liststudent.Refresh();
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            loadfull = false;
            toolStripMenuItem3.Checked = loadfull;
            toolStripMenuItem4.Checked = !loadfull;
            if (comboBox_listcourse.SelectedIndex < 0) return;
            if (lastStudent == null || lessonList == null || lastLogStudent == null) return;
            init_listView(lessonList, lastLogStudent, loadfull);
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            loadfull = true;
            toolStripMenuItem3.Checked = loadfull;
            toolStripMenuItem4.Checked = !loadfull;
            if (comboBox_listcourse.SelectedIndex < 0) return;
            if (lastStudent == null || lessonList == null || lastLogStudent == null) return;
            init_listView(lessonList, lastLogStudent, loadfull);
        }

        private void listView_chitiet_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (listView_chitiet.FocusedItem.Bounds.Contains(e.Location) == true)
                {
                    contextMenuStrip1.Show(Cursor.Position);
                }
            }
        }

        private void handmade(ListViewItem item = null, int timeType = 0)
        {

            if (lastSC == null)
            {
                MessageBox.Show("Không thể thêm dữ liệu!");
                return;
            }

            int No = tietHienTai;
            if (item != null) No = int.Parse(item.Text);

            DateTime dt = lessonList[No - 1].fullTimeStart;
            if (timeType == 1) dt = dt.AddMinutes(US.minLate + 5);
            if (Program.db.Tracking.Any(x => x.idSC == lastSC.idSC && x.No == No))
            {
                // Update Logs
                var log = Program.db.Tracking.FirstOrDefault(x => x.idSC == lastSC.idSC && x.No == No);
                if (timeType != 2)
                {
                    log.Logs = dt;
                    log.isLate = timeType == 0 ? "0" : (US.minLate + 5)+"";
                    log.Img = F.image2ByteArray(Properties.Resources.notepad);
                }
                else
                {
                    Program.db.Tracking.Remove(log);
                }
            }
            else if (timeType != 2)
            {
                // Add Logs
                var log = new Data.Tracking();
                log.idSC = lastSC.idSC;
                log.Logs = dt;
                log.isLate = timeType == 0 ? "0" :  (US.minLate + 5)+"";
                log.No = No;
                log.Img = null;
                Program.db.Tracking.Add(log);
            }


            if (item != null)
            {

                for (int i = 0; i < 3; i++)
                {
                    if (ARRAY_COLOR[i] == item.BackColor)
                    {
                        switch (timeType)
                        {
                            case 0:
                                if (i == 1) --tietTre;
                                if (i == 2) --tietVang;
                                break;
                            case 1:
                                if (i == 0) ++tietTre;
                                if (i == 2) { --tietVang; ++tietTre; }
                                break;
                            case 2:
                                if (i == 0) ++tietVang;
                                if (i == 1) { --tietTre; ++tietVang; }
                                break;
                        }
                        break;
                    }
                }
            }
            else
            {
                //Program.db.SaveChanges();
                //init_listView(lessonList, lastLogStudent, loadfull);
                init_lessonList(lastStudent, false);
            }

            setLessionCount(); // savechange <<
            var cItem = item;
            if (item == null) cItem = listView_chitiet.Items[tietHienTai - 1];
            // change view
            if (timeType == 2) cItem.SubItems[3].Text = "";
            else cItem.SubItems[3].Text = dt.ToString("HH:mm");
            cItem.BackColor = ARRAY_COLOR[timeType];

            // set liststudent
            if (No == tietHienTai && item != null)
            {
                foreach (DataGridViewRow row in dataGridView_liststudent.Rows)
                {
                    if (row.Cells[0].Value.ToString() == lastStudent.idStudent)
                    {
                        var cel = row.Cells[3] as DataGridViewComboBoxCell;
                        cel.Value = cel.Items[timeType];
                        cel.Style.BackColor = ARRAY_COLOR[timeType];
                        break;
                    }
                }
            }

        }

        private void toolStripMenuItem__Click_1(object sender, EventArgs e)
        {
            if (listView_chitiet.SelectedItems.Count == 0) return;
            int timeType = int.Parse(((ToolStripMenuItem)sender).Tag.ToString());
            foreach (ListViewItem item in listView_chitiet.SelectedItems)
            {
                handmade(item, timeType);
            }
        }

        private void Tracking_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Program.db.SaveChanges();
        }

        private void dataGridView_liststudent_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (dataGridView_liststudent.CurrentCell.ColumnIndex == 3 && e.Control is ComboBox)
            {
                ComboBox comboBox = e.Control as ComboBox;
                comboBox.DropDown += ComboBox_DropDown;
                comboBox.SelectedIndexChanged += SelectedIndexChanged;
            }
        }


        private void SelectedIndexChanged(object sender, EventArgs e)
        {
            var currentcell = dataGridView_liststudent.CurrentCellAddress;
            var sendingCB = sender as DataGridViewComboBoxEditingControl;
            var cel = dataGridView_liststudent.Rows[currentcell.Y].Cells[3];

            cel.Style.BackColor = ARRAY_COLOR[sendingCB.SelectedIndex];
            handmade(null, sendingCB.SelectedIndex);

        }

        private string lastDropdownValue = "";
        private void ComboBox_DropDown(object sender, EventArgs e)
        {
            var currentcell = dataGridView_liststudent.CurrentCellAddress;
            var cel = dataGridView_liststudent.Rows[currentcell.Y].Cells[3];
            lastDropdownValue = cel.Value.ToString();
        }


        private int getIndexColor(string value)
        {
            for (int i = 0; i <= 3; i++)
                if (ARRAY_NAME[i] == value) return i;
            return 0;
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            toolStripButton1_Click(null, null);
            if (dataGridView_liststudent.Rows.Count == 0 || dataGridView_liststudent.SelectedRows.Count != 1)
            {
                MessageBox.Show("Không tìm thấy dữ liệu!\nHãy chọn một sinh viên ở danh sách Sinh viên.", "Thông báo");
                return;
            }
            var save = new SaveFileDialog();
            save.FileName = lastStudent.idStudent + "-" + lastStudent.S_Name + ".xlsx";
            save.Filter = "EXCEL|*.xlsx";
            if (save.ShowDialog() == DialogResult.OK)
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                getReportStudent().ExportToExcel(save.FileName, lastCourse);
                watch.Stop();
                Console.WriteLine("gen DATA + export:" + watch.ElapsedMilliseconds);
            }
        }

        private DataTable getReportCourse()
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();

            string[] _field = new string[] { "STT", "ID", "Họ Tên", "Lớp sinh hoạt", "Ngày sinh" };
            DataTable dt = new DataTable();
            foreach (var field in _field) dt.Columns.Add(field);
            int stt = 0;
            foreach (DataGridViewRow row in dataGridView_liststudent.Rows)
            {
                DataRow dr = dt.NewRow();
                dr[_field[0]] = ++stt;
                for (int i = _field.Length - 2; i > 0; i--) dr[_field[i]] = row.Cells[i - 1].Value.ToString();
                dr[_field[4]] = studentList.ElementAt(stt - 1).S_Dob.ToString("yyyy-MM-dd");
                dt.Rows.Add(dr);
            }
            DataRow dr_dihoc = dt.NewRow(), dr_vanghoc = dt.NewRow();
            dr_dihoc[_field[4]] = "Đi học";
            dr_vanghoc[_field[4]] = "Vắng học";
            dt.Rows.Add(dr_dihoc);
            dt.Rows.Add(dr_vanghoc);

            var countLestion = 0;
            var columnIndex = 5;
            var lengthStudentList = dataGridView_liststudent.Rows.Count;
            foreach (var ls in lessonList)
            {
                string header = "(" + ++countLestion + ")\n" + ls.time.getLessonTimeStart() + "\n" + ls.ToString();
                dt.Columns.Add(header);
                var rowIndex = 0;
                var countAbsentAll = 0;
                foreach (var st in studentList)
                {
                    var sc = st.STUDENT_COURSE.SingleOrDefault(x =>
                            x.idCourse == comboBox_listcourse.SelectedValue.ToString() &&
                            x.SchoolYear == comboBox_schoolyear.Text);
                    var log = Program.db.Tracking.FirstOrDefault(x => x.idSC == sc.idSC && x.No == countLestion);
                    string text = "Vắng";
                    if (log != null)
                    {
                        if (log.isLate == null) text = "Vắng";
                        else if (log.isLate == "0") text = "Đúng giờ";
                        else text = "Trễ " + log.isLate + " phút";
                    }
                    if (text == "Vắng") ++countAbsentAll;
                    dt.Rows[rowIndex++][columnIndex] = text;
                }
                dt.Rows[rowIndex++][columnIndex] = lengthStudentList - countAbsentAll;
                dt.Rows[rowIndex++][columnIndex] = countAbsentAll;
                ++columnIndex;
            }

            dt.Columns.Add("Số tiết vắng / Tổng số tiết");
            dt.Columns.Add("Số tiết đi trễ / Tổng số tiết đi học");

            var lengthLesstionList = lessonList.Count;
            for (int r = 0; r < lengthStudentList; r++)
            {
                var countAbsentOne = 0;
                var countLateOne = 0;
                for (int c = 5; c < 5 + lengthLesstionList; c++)
                {
                    var value = dt.Rows[r][c].ToString();
                    if (value == "Vắng")
                    {
                        ++countAbsentOne;
                    }
                    else if (value.StartsWith("Trễ"))
                    {
                        ++countLateOne;
                    }
                }
                dt.Rows[r][5 + lengthLesstionList] = "'" + countAbsentOne + " / " + lengthLesstionList;
                dt.Rows[r][6 + lengthLesstionList] = "'" + countLateOne + " / " + (lengthLesstionList - countAbsentOne);
            }

            watch.Stop();
            Console.WriteLine("gen DATA:" + watch.ElapsedMilliseconds);
            return dt;
        }

        private DataTable getReportStudent()
        {
            return null;
        }

        private void listView_chitiet_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var itm = listView_chitiet.SelectedItems[0];
            var lastLesson = lessonList.ElementAt(itm.Index);
            var no = int.Parse(itm.Text);
            var tra = Program.db.Tracking.FirstOrDefault(t => t.idSC == lastSC.idSC && t.No == no);
            var img = tra == null ? null : F.byteArray2Image(tra.Img);
            var gdd = itm.SubItems[3].Text;
            var obj = new LessonDetail()
            {
                student = lastStudent,
                lesson = lastLesson,
                vangtong = label_vangtong.Text,
                muonhoc = label_muonhoc.Text,
                item = itm,
                image = img,
                giodiemdanh = gdd == "" ? "_" : gdd
            };
            new TrackingDetailLession(obj).ShowDialog();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            toolStripButton1_Click(null, null);
            if (dataGridView_liststudent.Rows.Count == 0)
            {
                MessageBox.Show("Không tìm thấy dữ liệu!", "Thông báo");
                return;
            }

            var save = new SaveFileDialog();
            var a = courseList.FirstOrDefault(c => c.idCourse == comboBox_listcourse.SelectedValue + "");
            save.FileName = comboBox_listcourse.SelectedValue + "-" + a.CourseName + ".xlsx";
            save.Filter = "EXCEL|*.xlsx";
            if (save.ShowDialog() == DialogResult.OK)
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                getReportCourse().ExportToExcel(save.FileName, lastCourse);
                watch.Stop();
                Console.WriteLine("gen DATA + export:" + watch.ElapsedMilliseconds);
            }
        }

        private void dataGridView_liststudent_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView_liststudent.SelectedRows.Count > 0)
            {
                var idStudent = dataGridView_liststudent.Rows[e.RowIndex].Cells["Column_idStudent"].Value.ToString();
                lastStudent = (from sv1 in studentList where sv1.idStudent == idStudent select sv1).FirstOrDefault();
                init_detail(lastStudent);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            textBox_gio.Text = string.Format("Thời Gian: {0}", DateTime.Now.ToString("HH:mm"));
            textBox_thungay.Text = (new LessonDate(DateTime.Now)).ToString();
        }
    }
}
