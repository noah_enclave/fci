﻿using System;
using System.Windows.Forms;

namespace FCI.View
{
    public partial class SystemSetting : Form
    {
        public SystemSetting()
        {
            InitializeComponent();
        }

        private void SystemSetting_Load(object sender, EventArgs e)
        {
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            //if (treeView1.SelectedNode == null) return;

            splitContainer2.Panel2.Controls.Clear();
            switch (treeView1.SelectedNode.Index)
            {

                case 0:
                    label1.Text = "Thiết lập Thời gian";
                    splitContainer2.Panel2.Controls.Add(new Control.TimeSetting());
                    break;

                case 1:
                    label1.Text = "Thiết lập Nhận diện";
                    splitContainer2.Panel2.Controls.Add(new Control.DetectParam());
                    break;

                case 2:
                    label1.Text = "Thiết lập Camera";
                    splitContainer2.Panel2.Controls.Add(new Control.CameraSetting());
                    break;

            }
        }
    }
}
