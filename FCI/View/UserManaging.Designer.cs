﻿namespace FCI.View
{
    partial class UserManaging
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserManaging));
            this.textBox_idAccount = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_Username = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox_roles = new System.Windows.Forms.ComboBox();
            this.textBox_value = new System.Windows.Forms.TextBox();
            this.dataGridView_tk = new System.Windows.Forms.DataGridView();
            this.Column_idAccount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_username = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.checkBox_status = new System.Windows.Forms.CheckBox();
            this.comboBox_Department = new System.Windows.Forms.ComboBox();
            this.comboBox_role = new System.Windows.Forms.ComboBox();
            this.comboBox_Faculty = new System.Windows.Forms.ComboBox();
            this.textBox_idInstructor = new System.Windows.Forms.TextBox();
            this.textBox_I_Phone = new System.Windows.Forms.TextBox();
            this.textBox_I_Email = new System.Windows.Forms.TextBox();
            this.textBox_password2 = new System.Windows.Forms.TextBox();
            this.textBox_I_Name = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox_password = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.comboBox_status = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.textBox_trangthai = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_tk)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox_idAccount
            // 
            this.textBox_idAccount.Location = new System.Drawing.Point(112, 48);
            this.textBox_idAccount.Name = "textBox_idAccount";
            this.textBox_idAccount.ReadOnly = true;
            this.textBox_idAccount.Size = new System.Drawing.Size(196, 21);
            this.textBox_idAccount.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "ID tài khoản:";
            // 
            // textBox_Username
            // 
            this.textBox_Username.Location = new System.Drawing.Point(112, 77);
            this.textBox_Username.Name = "textBox_Username";
            this.textBox_Username.Size = new System.Drawing.Size(196, 21);
            this.textBox_Username.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tài khoản:";
            // 
            // comboBox_roles
            // 
            this.comboBox_roles.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.comboBox_roles.DisplayMember = "RoleName";
            this.comboBox_roles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_roles.FormattingEnabled = true;
            this.comboBox_roles.Location = new System.Drawing.Point(34, 53);
            this.comboBox_roles.Name = "comboBox_roles";
            this.comboBox_roles.Size = new System.Drawing.Size(193, 23);
            this.comboBox_roles.TabIndex = 1;
            this.comboBox_roles.ValueMember = "idRole";
            this.comboBox_roles.SelectedIndexChanged += new System.EventHandler(this.comboBox_role_SelectedIndexChanged);
            // 
            // textBox_value
            // 
            this.textBox_value.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textBox_value.Location = new System.Drawing.Point(99, 99);
            this.textBox_value.Name = "textBox_value";
            this.textBox_value.Size = new System.Drawing.Size(244, 21);
            this.textBox_value.TabIndex = 3;
            this.textBox_value.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox4_KeyDown);
            // 
            // dataGridView_tk
            // 
            this.dataGridView_tk.AllowUserToAddRows = false;
            this.dataGridView_tk.AllowUserToDeleteRows = false;
            this.dataGridView_tk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.dataGridView_tk.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_tk.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column_idAccount,
            this.Column_username,
            this.Column_name});
            this.dataGridView_tk.Location = new System.Drawing.Point(34, 126);
            this.dataGridView_tk.MultiSelect = false;
            this.dataGridView_tk.Name = "dataGridView_tk";
            this.dataGridView_tk.ReadOnly = true;
            this.dataGridView_tk.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_tk.Size = new System.Drawing.Size(363, 362);
            this.dataGridView_tk.TabIndex = 0;
            this.dataGridView_tk.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_tk_RowEnter);
            // 
            // Column_idAccount
            // 
            this.Column_idAccount.DataPropertyName = "idAccount";
            this.Column_idAccount.HeaderText = "idAccount";
            this.Column_idAccount.Name = "Column_idAccount";
            this.Column_idAccount.ReadOnly = true;
            this.Column_idAccount.Visible = false;
            // 
            // Column_username
            // 
            this.Column_username.DataPropertyName = "UserName";
            this.Column_username.HeaderText = "Tài khoản";
            this.Column_username.Name = "Column_username";
            this.Column_username.ReadOnly = true;
            this.Column_username.Width = 150;
            // 
            // Column_name
            // 
            this.Column_name.DataPropertyName = "I_Name";
            this.Column_name.HeaderText = "Giảng Viên";
            this.Column_name.Name = "Column_name";
            this.Column_name.ReadOnly = true;
            this.Column_name.Width = 150;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(35, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 15);
            this.label4.TabIndex = 1;
            this.label4.Text = "Tìm kiếm";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(31, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 15);
            this.label5.TabIndex = 1;
            this.label5.Text = "Quyền hạn";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.checkBox_status);
            this.panel1.Controls.Add(this.comboBox_Department);
            this.panel1.Controls.Add(this.comboBox_role);
            this.panel1.Controls.Add(this.comboBox_Faculty);
            this.panel1.Controls.Add(this.textBox_idInstructor);
            this.panel1.Controls.Add(this.textBox_idAccount);
            this.panel1.Controls.Add(this.textBox_I_Phone);
            this.panel1.Controls.Add(this.textBox_I_Email);
            this.panel1.Controls.Add(this.textBox_password2);
            this.panel1.Controls.Add(this.textBox_I_Name);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.textBox_password);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.textBox_Username);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(403, 25);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(333, 488);
            this.panel1.TabIndex = 5;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(282, 105);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(26, 23);
            this.button2.TabIndex = 9;
            this.button2.Text = "?";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(210, 443);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(98, 35);
            this.button1.TabIndex = 19;
            this.button1.Text = "Cập nhật";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label9.Location = new System.Drawing.Point(109, 232);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(153, 18);
            this.label9.TabIndex = 3;
            this.label9.Text = "Thông tin Giảng Viên";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label8.Location = new System.Drawing.Point(109, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(138, 18);
            this.label8.TabIndex = 3;
            this.label8.Text = "Thông tin tài khoản";
            // 
            // checkBox_status
            // 
            this.checkBox_status.AutoSize = true;
            this.checkBox_status.Location = new System.Drawing.Point(112, 193);
            this.checkBox_status.Name = "checkBox_status";
            this.checkBox_status.Size = new System.Drawing.Size(83, 19);
            this.checkBox_status.TabIndex = 12;
            this.checkBox_status.Text = "Hoạt động";
            this.checkBox_status.UseVisualStyleBackColor = true;
            // 
            // comboBox_Department
            // 
            this.comboBox_Department.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.comboBox_Department.DisplayMember = "DepartmentName";
            this.comboBox_Department.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Department.FormattingEnabled = true;
            this.comboBox_Department.Location = new System.Drawing.Point(112, 405);
            this.comboBox_Department.Name = "comboBox_Department";
            this.comboBox_Department.Size = new System.Drawing.Size(196, 23);
            this.comboBox_Department.TabIndex = 18;
            this.comboBox_Department.ValueMember = "idDepartment";
            // 
            // comboBox_role
            // 
            this.comboBox_role.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.comboBox_role.DisplayMember = "RoleName";
            this.comboBox_role.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_role.FormattingEnabled = true;
            this.comboBox_role.Location = new System.Drawing.Point(112, 164);
            this.comboBox_role.Name = "comboBox_role";
            this.comboBox_role.Size = new System.Drawing.Size(196, 23);
            this.comboBox_role.TabIndex = 11;
            this.comboBox_role.ValueMember = "idRole";
            // 
            // comboBox_Faculty
            // 
            this.comboBox_Faculty.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.comboBox_Faculty.DisplayMember = "FacultyName";
            this.comboBox_Faculty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Faculty.FormattingEnabled = true;
            this.comboBox_Faculty.Location = new System.Drawing.Point(112, 376);
            this.comboBox_Faculty.Name = "comboBox_Faculty";
            this.comboBox_Faculty.Size = new System.Drawing.Size(196, 23);
            this.comboBox_Faculty.TabIndex = 17;
            this.comboBox_Faculty.ValueMember = "idFaculty";
            this.comboBox_Faculty.SelectedIndexChanged += new System.EventHandler(this.comboBox_Faculty_SelectedIndexChanged);
            // 
            // textBox_idInstructor
            // 
            this.textBox_idInstructor.Location = new System.Drawing.Point(112, 262);
            this.textBox_idInstructor.Name = "textBox_idInstructor";
            this.textBox_idInstructor.ReadOnly = true;
            this.textBox_idInstructor.Size = new System.Drawing.Size(196, 21);
            this.textBox_idInstructor.TabIndex = 13;
            // 
            // textBox_I_Phone
            // 
            this.textBox_I_Phone.Location = new System.Drawing.Point(112, 349);
            this.textBox_I_Phone.Name = "textBox_I_Phone";
            this.textBox_I_Phone.Size = new System.Drawing.Size(196, 21);
            this.textBox_I_Phone.TabIndex = 16;
            // 
            // textBox_I_Email
            // 
            this.textBox_I_Email.Location = new System.Drawing.Point(112, 320);
            this.textBox_I_Email.Name = "textBox_I_Email";
            this.textBox_I_Email.Size = new System.Drawing.Size(196, 21);
            this.textBox_I_Email.TabIndex = 15;
            // 
            // textBox_password2
            // 
            this.textBox_password2.Location = new System.Drawing.Point(112, 135);
            this.textBox_password2.Name = "textBox_password2";
            this.textBox_password2.PasswordChar = '*';
            this.textBox_password2.Size = new System.Drawing.Size(196, 21);
            this.textBox_password2.TabIndex = 10;
            // 
            // textBox_I_Name
            // 
            this.textBox_I_Name.Location = new System.Drawing.Point(112, 291);
            this.textBox_I_Name.Name = "textBox_I_Name";
            this.textBox_I_Name.Size = new System.Drawing.Size(196, 21);
            this.textBox_I_Name.TabIndex = 14;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(24, 352);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(82, 15);
            this.label15.TabIndex = 1;
            this.label15.Text = "Số điện thoại:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(41, 194);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(65, 15);
            this.label17.TabIndex = 1;
            this.label17.Text = "Trạng thái:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(37, 167);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(69, 15);
            this.label16.TabIndex = 1;
            this.label16.Text = "Quyền hạn:";
            // 
            // textBox_password
            // 
            this.textBox_password.Location = new System.Drawing.Point(112, 106);
            this.textBox_password.Name = "textBox_password";
            this.textBox_password.PasswordChar = '*';
            this.textBox_password.Size = new System.Drawing.Size(164, 21);
            this.textBox_password.TabIndex = 8;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(64, 379);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(39, 15);
            this.label13.TabIndex = 1;
            this.label13.Text = "Khoa:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(25, 268);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(81, 15);
            this.label12.TabIndex = 1;
            this.label12.Text = "ID giảng viên:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(64, 323);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 15);
            this.label14.TabIndex = 1;
            this.label14.Text = "Email:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(27, 138);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 15);
            this.label7.TabIndex = 1;
            this.label7.Text = "Nhập lại MK:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(53, 408);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 15);
            this.label11.TabIndex = 1;
            this.label11.Text = "Bộ môn:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 294);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 15);
            this.label10.TabIndex = 1;
            this.label10.Text = "Tên giảng viên:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(47, 109);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 15);
            this.label6.TabIndex = 1;
            this.label6.Text = "Mật khẩu:";
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(230, 35);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(62, 15);
            this.label18.TabIndex = 1;
            this.label18.Text = "Trạng thái";
            // 
            // comboBox_status
            // 
            this.comboBox_status.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.comboBox_status.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_status.FormattingEnabled = true;
            this.comboBox_status.Items.AddRange(new object[] {
            "Từ chối sử dụng",
            "Đang hoạt động"});
            this.comboBox_status.Location = new System.Drawing.Point(233, 53);
            this.comboBox_status.Name = "comboBox_status";
            this.comboBox_status.Size = new System.Drawing.Size(164, 23);
            this.comboBox_status.TabIndex = 2;
            this.comboBox_status.SelectedIndexChanged += new System.EventHandler(this.comboBox_role_SelectedIndexChanged);
            // 
            // button3
            // 
            this.button3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Image = global::FCI.Properties.Resources.searchbutton;
            this.button3.Location = new System.Drawing.Point(372, 97);
            this.button3.Margin = new System.Windows.Forms.Padding(0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(25, 25);
            this.button3.TabIndex = 4;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button4.Location = new System.Drawing.Point(343, 98);
            this.button4.Margin = new System.Windows.Forms.Padding(0);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(23, 23);
            this.button4.TabIndex = 5;
            this.button4.Text = "X";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // textBox_trangthai
            // 
            this.textBox_trangthai.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textBox_trangthai.Location = new System.Drawing.Point(34, 494);
            this.textBox_trangthai.Name = "textBox_trangthai";
            this.textBox_trangthai.ReadOnly = true;
            this.textBox_trangthai.Size = new System.Drawing.Size(363, 21);
            this.textBox_trangthai.TabIndex = 0;
            this.textBox_trangthai.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox4_KeyDown);
            // 
            // UserManaging
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(756, 525);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridView_tk);
            this.Controls.Add(this.comboBox_status);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.comboBox_roles);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox_trangthai);
            this.Controls.Add(this.textBox_value);
            this.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UserManaging";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lý tài khoản";
            this.Load += new System.EventHandler(this.UserManaging_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_tk)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_idAccount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_Username;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox_roles;
        private System.Windows.Forms.TextBox textBox_value;
        private System.Windows.Forms.DataGridView dataGridView_tk;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBox_password2;
        private System.Windows.Forms.TextBox textBox_password;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox checkBox_status;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox_idInstructor;
        private System.Windows.Forms.TextBox textBox_I_Name;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox_I_Phone;
        private System.Windows.Forms.TextBox textBox_I_Email;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox comboBox_Department;
        private System.Windows.Forms.ComboBox comboBox_Faculty;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox comboBox_role;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox comboBox_status;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_idAccount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_username;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_name;
        private System.Windows.Forms.TextBox textBox_trangthai;
    }
}