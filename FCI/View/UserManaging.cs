﻿using FCI.Func;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FCI.View
{
    public partial class UserManaging : Form
    {
        private object listAccount = null;
        private bool showpass = false;

        public UserManaging()
        {
            InitializeComponent();
        }

        private void UserManaging_Load(object sender, EventArgs e)
        {
            load_combobox();
            comboBox_status.SelectedIndex = 1;

        }

        private void load_combobox()
        {
            var roles = new List<Data.Role>();
            roles.Add(new Data.Role { idRole = -1, RoleName = "[Tất cả]" });
            roles.AddRange(Program.db.Role.ToList());
            comboBox_roles.DataSource = roles;
            comboBox_role.DataSource = Program.db.Role.ToList();
            comboBox_role.SelectedIndex = -1;
            comboBox_Faculty.DataSource = Program.db.Faculty.ToList();
            comboBox_Faculty.SelectedIndex = -1;
        }

        private void comboBox_role_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (textBox_value.Text != "")
            {
                button3_Click(null, null);
                return;
            }
            var idRole = int.Parse(comboBox_roles.SelectedValue.ToString());
            bool all = (idRole == -1);
            listAccount = (from a in Program.db.Account
                           join i in Program.db.Instructor on a.idAccount equals i.I_idAccount
                           where (all || a.idRole == idRole)
                           && a.Status == (comboBox_status.SelectedIndex == 1)
                           select new { a.idAccount, a.UserName, i.I_Name }).ToList();
            dataGridView_tk.DataSource = listAccount;
            textBox_trangthai.Text = "Tổng số: " + dataGridView_tk.Rows.Count + " " + comboBox_roles.Text + ", " + comboBox_status.Text;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var value = textBox_value.Text.ToLower().Trim();
            if (value == "")
            {
                dataGridView_tk.DataSource = listAccount;
            }
            else
            {
                if (dataGridView_tk.DataSource == null || dataGridView_tk.Rows.Count == 0) return;
                var idRole = int.Parse(comboBox_roles.SelectedValue.ToString());
                bool all = (idRole == -1);
                dataGridView_tk.DataSource = (from a in Program.db.Account
                                              join i in Program.db.Instructor on a.idAccount equals i.I_idAccount
                                              where (all || a.idRole == idRole)
                                              && a.Status == (comboBox_status.SelectedIndex == 1)
                                              && (a.UserName.ToLower().Contains(value)
                                              || i.I_Name.ToLower().Contains(value))
                                              select new { a.idAccount, a.UserName, i.I_Name }).ToList();
            }
        }

        private void textBox4_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button3_Click(null, null);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox_value.Text = "";
            dataGridView_tk.DataSource = listAccount;
        }

        private void dataGridView_tk_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView_tk.SelectedRows.Count > 0)
            {
                var idAccount = int.Parse(dataGridView_tk.Rows[e.RowIndex].Cells["Column_idAccount"].Value.ToString());
                init_detail(idAccount);
            }
        }

        private void init_detail(int idAccount)
        {
            var tk = Program.db.Account.FirstOrDefault(a => a.idAccount == idAccount);
            var gv = Program.db.Instructor.FirstOrDefault(i => i.I_idAccount == tk.idAccount);
            // tai khoan
            textBox_idAccount.Text = tk.idAccount + "";
            textBox_Username.Text = tk.UserName + "";
            textBox_password.Text = textBox_password2.Text = tk.Password;
            comboBox_role.SelectedValue = tk.idRole;
            checkBox_status.Checked = tk.Status == true;
            // giang vien
            textBox_idInstructor.Text = gv.idInstructor;
            textBox_I_Name.Text = gv.I_Name;
            textBox_I_Email.Text = gv.I_Email;
            textBox_I_Phone.Text = gv.I_Phone;

            var idF = Program.db.Department.FirstOrDefault(d => d.idDepartment == gv.I_idDepartment).idFaculty;
            comboBox_Faculty.SelectedValue = idF;
            comboBox_Department.SelectedValue = gv.I_idDepartment;

        }
        private void button2_Click(object sender, EventArgs e)
        {
            textBox_password.PasswordChar = (showpass = !showpass) ? '\0' : '*';
        }

        private void comboBox_Faculty_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox_Faculty.SelectedIndex < 0 || textBox_idInstructor.Text == "") return;
            comboBox_Department.DataSource = Program.db.Department.Where(d => d.idFaculty == comboBox_Faculty.SelectedValue + "").ToList();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (textBox_idAccount.Text == "" || textBox_idInstructor.Text == "")
            {
                MessageBox.Show("Không có tài khoản nào được chọn", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            // get values
            int idAccount = int.Parse(textBox_idAccount.Text);
            string username = textBox_Username.Text;
            string password = textBox_password.Text;
            string password2 = textBox_password2.Text;
            int idRole = int.Parse(comboBox_role.SelectedValue + "");
            bool status = checkBox_status.Checked;

            string idInstructor = textBox_idInstructor.Text;
            string iName = textBox_I_Name.Text;
            string iEmail = textBox_I_Email.Text;
            string iPhone = textBox_I_Phone.Text;
            string idDepartment = comboBox_Department.SelectedValue + "";

            if (F.checkUserName(username)) return;
            if (F.checkExist(idAccount, username)) return;
            if (F.checkPassword(password, password2)) return;
            if (F.checkName(ref iName)) return;
            if (F.checkEmail(iEmail)) return;
            if (F.checkPhone(ref iPhone)) return;

            // update account

            var a = Program.db.Account.FirstOrDefault(x => x.idAccount == idAccount);
            a.UserName = username;
            a.Password = password;
            a.idRole = idRole;
            a.Status = status;

            var i = Program.db.Instructor.FirstOrDefault(x => x.idInstructor == idInstructor);
            i.I_Name = iName;
            i.I_Email = iEmail;
            i.I_Phone = iPhone;
            i.I_idDepartment = idDepartment;

            try {
                Program.db.SaveChanges();
                MessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show("Đã xảy ra lỗi khi lưu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            var index = dataGridView_tk.SelectedRows[0].Index;
            comboBox_role_SelectedIndexChanged(null, null);
            if (dataGridView_tk.Rows.Count > index)
                dataGridView_tk.CurrentCell = dataGridView_tk.Rows[index].Cells[1];
        }

    }
}
