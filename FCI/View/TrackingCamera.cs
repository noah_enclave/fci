﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using FCI.Func;
using FCI.Struct;
using FCI.Data;
using System.Threading;

namespace FCI.View
{
    public partial class TrackingCamera : Form
    {

        #region variables
        Image<Bgr, byte> currentFrame;
        Image<Gray, byte> result;
        Image<Gray, byte> gray_frame = null;
        ImageList imageList = new ImageList();
        MCvFont font = new MCvFont(FONT.CV_FONT_HERSHEY_SIMPLEX, 0.3, 0.4);
        //double maxpercent = 0;

        //Classifier with default training location
        Func.Classifier_Trained _Recog = null;

        private Bgr[] boderColor = new Bgr[] { new Bgr(Color.Red), new Bgr(Color.Green), new Bgr(Color.Yellow) };
        Dictionary<string, int> AC = new Dictionary<string, int>();
        string recres="", id="", lastId = "", name="";
        Dictionary<string, int> stl = new Dictionary<string, int>();
        System.Media.SoundPlayer correctSound = null;

        // Tracking tracking = null;

        private IEnumerable<Data.Course> courseList;
        private IEnumerable<Data.Student> studentList;
        private List<Lesson> lessonList = new List<Lesson>();
        private int tietHienTai, tietTong;
        public static bool opening = false;
        private int yearIndex, courseIndex;
        #endregion

        public TrackingCamera(int yearIndex = -1, int courseIndex = -1)
        {
            this.yearIndex = yearIndex;
            this.courseIndex = courseIndex;
            InitializeComponent();
        }

        // using for count frame
        private IEnumerable<Data.Student> init_st(IEnumerable<Data.Student> stl_in)
        {
            if (stl_in == null) return null;
            foreach (var st_in in stl_in)
            {
                if (!stl.ContainsKey(st_in.idStudent)) stl.Add(st_in.idStudent, 0);
            }
            return stl_in;
        }

        private void Tracking_Camera_Load(object sender, EventArgs e)
        {
            opening = true;

            // Progra,
            stop_capture();
            //
            correctSound = new System.Media.SoundPlayer(Application.StartupPath + @"\Resources\beep.wav");

            // Setting infomation
            textBox_gio.Text = string.Format("Thời Gian: {0}", DateTime.Now.ToString("HH:mm"));
            textBox_thungay.Text = (new LessonDate(DateTime.Now)).ToString();


            // Setting image list
            imageList.ImageSize = new Size(55, 55);
            listView_image.LargeImageList = imageList;
            listView_image.View = System.Windows.Forms.View.Tile;
            refreshListViewImage();

            //
            loadYear();
            //
            AC[""] = 0;

        }

        private void loadYear()
        {
            comboBox_schoolyear.Items.AddRange(F.yearSchool());
            if (yearIndex != -1)
            {
                comboBox_schoolyear.SelectedIndex = yearIndex;
                yearIndex = -1;
                return;
            }
            var t = DateTime.Now.Month;
            var n = DateTime.Now.Year;
            string sy = "";
            if (t < 8) sy = (n - 1) + "-" + n;
            else sy = n + "-" + (n + 1);
            comboBox_schoolyear.SelectedItem = sy;
        }

        private void refreshListViewImage()
        {
            listView_image.Clear();
            for (int i = 0; i < imageList.Images.Count; i++)
            {
                listView_image.AddRow(i, imageList.Images.Keys[i]);
            }
        }


        private void Tracking_Camera_FormClosed(object sender, FormClosedEventArgs e)
        {
            opening = false;
            stop_capture();
            Program.db.SaveChanges();
            //Program.threadCamera.Abort();
        }

        //Camera Start Stop
        public void init_capture()
        {
            Program.grabber = new Capture(US.cameraID);
            Program.grabber.QueryFrame();
            Program.grabber.FlipHorizontal = true;
            //Initialize the FrameGraber event
            Application.Idle += new EventHandler(FrameGrabber_Parrellel);
        }
        private void stop_capture()
        {
            Application.Idle -= new EventHandler(FrameGrabber_Parrellel);
            if (Program.grabber != null)
            {
                Program.grabber.Dispose();
                Program.grabber = null;
            }
        }

        //System.IO.StreamWriter file = new System.IO.StreamWriter(@"D:\log.txt", true);
        void FrameGrabber_Parrellel(object sender, EventArgs e)
        {

            //Get the current frame form capture device
            currentFrame = Program.grabber.QueryFrame().Resize(1, INTER.CV_INTER_CUBIC);
            //file.WriteLine("id:" + id++);
            //Convert it to Grayscale
            //Clear_Faces_Found();

            if (currentFrame != null)
            {
                gray_frame = currentFrame.Convert<Gray, byte>();

                //Face Detector
                var facesDetected = Program.faceLib.DetectMultiScale(gray_frame, 1.2, 10, new Size(50, 50), new Size(500, 500));

                //Parallel.ForEach(facesDetected, (face_found, state) =>
                foreach (Rectangle face_found in facesDetected)
                {
                   // try
                    {
                        result = currentFrame.Copy(face_found).Convert<Gray, byte>().Resize(200, 200, INTER.CV_INTER_CUBIC);
                        result._EqualizeHist();
                        if (_Recog.IsTrained)
                        {
                            recres = _Recog.Recognise(result);
                            if (recres != "")
                            {
                                id = recres.Split('@')[1];
                                name = recres.Split('@')[0];
                                if (lastId != id)
                                {
                                    if (stl[id] != US.numCap + 1)
                                    {
                                        stl[id] = 0;
                                    }
                                    lastId = id;
                                }
                                AC[lastId] = 1;
                                int count = stl[id];
                                if (count < US.numCap)
                                {
                                    ++stl[id];
                                }
                                else
                                {
                                    if (count == US.numCap)
                                    {
                                        ADD_Face_Found(result, currentFrame, name, id);
                                        correctSound.Play();
                                        ++stl[id];
                                        break;
                                    }
                                    AC[lastId] = 2;
                                }
                                currentFrame.Draw(face_found, boderColor[AC[lastId]], 1);
                                currentFrame.Draw((count < US.numCap ? "[" + (US.numCap - count - 1) + "]" : "") + name, ref font,
                                    new Point(face_found.X + 0, face_found.Y + face_found.Height + 10),
                                    boderColor[AC[lastId]]);
                            }
                            else
                            {
                                currentFrame.Draw(face_found, boderColor[0], 1);
                            }
                        }
                        else
                        {
                            currentFrame.Draw(face_found, boderColor[0], 1);
                        }
                    }
                 //   catch (Exception ex)
                    {
                   //     Console.WriteLine(ex.ToString());
                    }
                }//);
                image_PICBX.Image = currentFrame.ToBitmap();
            }
        }

        void ADD_Face_Found(Image<Gray, byte> img_found, Image<Bgr, byte> img_full, string name_person, string id, double match_value = 0)
        {

            var std = studentList.FirstOrDefault(s => s.idStudent == id);

            // Add listview
            listBox1.Items.Add("[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm") + "] [" + id + "] " + (std == null ? name_person : std.S_Name));

            // Add image
            imageList.Images.Add(name_person, img_found.ToBitmap());
            refreshListViewImage();

            // Add Logs
            var tracking = Program.db.STUDENT_COURSE.Where(x => x.idStudent == id
                && x.idCourse == comboBox_listcourse.SelectedValue.ToString()
                && x.SchoolYear == comboBox_schoolyear.Text).FirstOrDefault();

            if (tracking == null)
            {
                MessageBox.Show("Không thể thêm dữ liệu!");
                return;
            }


            var log = new Data.Tracking();
            var _isLate = (int)(DateTime.Now - lessonList[tietHienTai - 1].fullTimeStart).TotalMinutes;
            var isLate = _isLate <= 0 ? "0" : _isLate - US.minLate + "";

            log.idSC = tracking.idSC;
            log.Logs = DateTime.Now;
            log.isLate = isLate;
            log.No = tietHienTai;
            log.Img = F.image2ByteArray(img_full.Resize(0.5, INTER.CV_INTER_CUBIC).Bitmap);
            Program.db.Tracking.Add(log);

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            textBox_gio.Text = string.Format("Thời Gian: {0}", DateTime.Now.ToString("HH:mm"));
        }

        private void comboBox_schoolyear_SelectedIndexChanged(object sender, EventArgs e)
        {
            init_comboboxCourse();
        }

        private void comboBox_listcourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox_listcourse.SelectedIndex < 0) return;
            load_course(comboBox_listcourse.SelectedValue.ToString());
            init_gridviewStudent();
            // tuy thuoc vao muc tuy chinh
        }

        private void init_comboboxCourse()
        {
            //
            comboBox_listcourse.DataSource = courseList = (from cou in Program.db.Course
                                                           join ins in Program.db.Instructor on cou.idInstructor equals ins.idInstructor
                                                           join acc in Program.db.Account on ins.I_idAccount equals acc.idAccount
                                                           where acc.idAccount == Data.Session.idAccount && cou.SchoolYear == comboBox_schoolyear.Text
                                                           select cou).ToList();
            if (courseIndex != -1)
            {
                comboBox_listcourse.SelectedIndex = courseIndex;
                courseIndex = -1;
            }
        }
        private void init_gridviewStudent()
        {
            studentList = (from stu in Program.db.Student
                           join sc in Program.db.STUDENT_COURSE on stu.idStudent equals sc.idStudent
                           join cou in Program.db.Course on sc.idCourse equals cou.idCourse
                           where cou.idCourse == comboBox_listcourse.SelectedValue.ToString() && sc.SchoolYear == cou.SchoolYear
                           select stu).ToList();

            //.
            var cy = Program.db.STUDENT_COURSE.Where(stc =>
              stc.idCourse == comboBox_listcourse.SelectedValue.ToString() &&
              stc.SchoolYear == comboBox_schoolyear.Text);
            foreach (var stu in studentList)
            {
                var res = cy.Where(x => x.idStudent == stu.idStudent).First();
                if (res == null) continue;
                var has = Program.db.Tracking.FirstOrDefault(tra => tra.idSC == res.idSC && tra.No == tietHienTai);
                if (has != null && has.Logs != null)
                {
                    stl[res.idStudent] = US.numCap + 1;
                }
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            // check chọn tin chi? lop da bat dau? lop da ket thuc? buoi hoc da bat dau? buoi hoc da ket thuc?
            if (comboBox_listcourse.SelectedIndex < 0)
            {
                MessageBox.Show("Lớp tín chỉ chưa được chọn");
                return;
            }

            Enabled = false;
            if (Program.grabber == null)
            {
                if (!checkTimeOK())
                {
                    Enabled = true;
                    return;
                }
                try
                {
                    init_capture();
                }
                catch
                {
                    MessageBox.Show("Không thể khởi động Camera!\nHãy vào phần thiết lập hệ thống để cài đặt lại.");
                    Enabled = true;
                    return;
                }
                _Recog = new Classifier_Trained(init_st(studentList));
                comboBox_schoolyear.Enabled = false;
                comboBox_listcourse.Enabled = false;
                button1.Text = "Tắt Camera";
            }
            else
            {
                stop_capture();
                image_PICBX.Image = null;
                comboBox_schoolyear.Enabled = true;
                comboBox_listcourse.Enabled = true;
                button1.Text = "Bật Camera";

            }
            Enabled = true;

        }


        private bool checkTimeOK()
        {
            //TODO
            if (tietHienTai < 1)
            {
                MessageBox.Show("Khóa học chưa bắt đầu", "Thông báo");
                return false;
            }
            if (tietHienTai > tietTong)
            {
                MessageBox.Show("Khóa học đã kết thúc", "Thông báo");
                return false;
            }
            if (DateTime.Compare(DateTime.Now.AddMinutes(US.minEarly), lessonList[tietHienTai - 1].fullTimeStart) < 0)
            {
                MessageBox.Show("Tiết học chưa bắt đầu \n--------------" + getInfo(tietHienTai - 1), "Thông báo");
                return false;
            }
            if (DateTime.Compare(DateTime.Now, lessonList[tietHienTai - 1].fullTimeEnd) >= 0)
            {
                var addin = "";
                if (tietHienTai + 1 <= tietTong)
                    addin = "\n\nTiết học tiếp theo \n--------------" + getInfo(tietHienTai);
                MessageBox.Show("Tiết học đã kết thúc \n--------------" + getInfo(tietHienTai - 1) + addin, "Thông báo");
                return false;
            }
            return true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (comboBox_listcourse.SelectedIndex < 0) return;
            string info = "--------------";
            int index = tietHienTai - 1;
            if (index < 0) return;
            if (lessonList.Count > 1 && index >= 1)
            {
                info += getInfo(index - 1);
            }
            info += getInfo(index);
            if (lessonList.Count > index + 1)
            {
                info += getInfo(index + 1);
            }
            MessageBox.Show(info, "Thông tin");
        }
        private string getInfo(int index)
        {
            return "\nTiết\t\t: " + (index + 1)
                + "\nThời gian\t\t: " + lessonList.ElementAt(index).date
                + "\nGiờ bắt đầu\t: " + lessonList.ElementAt(index).time.getLessonTimeStart()
                + "\nGiờ kết thúc\t: " + lessonList.ElementAt(index).time.getLessonTimeEnd()
                + "\n--------------";
        }

        private void load_course(string idCourse)
        {
            var course = (from cou in courseList where cou.idCourse == idCourse select cou).FirstOrDefault();
            var sch = Program.db.Schedule.Where(sc => sc.idCourse == idCourse && sc.SchoolYear == comboBox_schoolyear.Text).FirstOrDefault();
            if (sch == null)
            {
                MessageBox.Show("Lớp tín chỉ chưa có lịch");
                return;
            }
            string[] dow = { sch.Sun, sch.Mon, sch.Tue, sch.Wed, sch.Thu, sch.Fri, sch.Sat, };
            lessonList.Clear();

            // init for student

            int lessonCount = 0;
            string[][] lessonInDayOfWeek = new string[7][];
            for (int i = 0; i < 7; i++) lessonInDayOfWeek[i] = new string[5];


            tietHienTai = 0;
            var now = DateTime.Now.AddMinutes(US.minEarly);

            foreach (DateTime day in F.EachDay(course.StartDate, course.EndDate))
            {
                var i = (int)day.DayOfWeek;
                if (lessonInDayOfWeek[i].Length == 5)
                {
                    if (dow[i] == "" || dow[i] == null) continue;
                    lessonInDayOfWeek[i] = dow[i].Split(';');
                }
                foreach (string lessonTime in lessonInDayOfWeek[i])
                {
                    if (lessonTime == "") continue;
                    lessonList.Add(new Lesson(++lessonCount, day, lessonTime));

                    if (DateTime.Compare(now, lessonList.Last().fullTimeStart) >= 0)
                    {
                        ++tietHienTai;
                    }
                }
            }
            tietTong = lessonList.Count;
            if (tietHienTai > 0)
            {
                textBox_tiethoc.Text = string.Format("Tiết học: {0} / {1}", tietHienTai, tietTong);
                var lessonNow = lessonList.ElementAt(tietHienTai - 1);
                textBox_giobd.Text = "Giờ bắt đầu:  " + lessonNow.time.getLessonTimeStart();
                textBox_giokt.Text = "Giờ kết thúc: " + lessonNow.time.getLessonTimeEnd();
            }
            else
            {
                textBox_tiethoc.Text = "Tiết học: _";
                textBox_giobd.Text = "Giờ bắt đầu: _";
                textBox_giokt.Text = "Giờ kết thúc: _";
            }
        }


    }
}
