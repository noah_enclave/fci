﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace FCI.View
{
    public partial class Accessing : Form
    {
        private int countLogin = 3;
        public Accessing()
        {
            InitializeComponent();
        }

        private void button_login_Click(object sender, EventArgs e)
        {
            var match = textBox_username.Text.ToLower() +"@"+ textBox_password.Text;
            var userlogin = Program.db.Account.Where(x => x.UserName.ToLower() +"@"+ x.Password == match).FirstOrDefault();
            if (userlogin == null)
            {
                MessageBox.Show("Sai Tên đăng nhập hoặc mật khẩu\nHãy thử lại!", "Đăng nhập từ chối", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBox_username.Focus();
                if (--countLogin > 0) return;
                DialogResult = DialogResult.Abort;
            }
            else
            {
                if (userlogin.Status != true)
                {
                    MessageBox.Show("Tài khoản này không được phép hoạt động.", "Thông báo");
                    return;
                }
                if (userlogin.idRole == 8)
                {
                    MessageBox.Show("Thanh Tra không được phép truy cập hệ thống.", "Thông báo");
                    return;
                }
                Data.Session.setSession(userlogin.idAccount, userlogin.idRole);
                DialogResult = DialogResult.OK;
            }
            Close();
        }

        private void textBox_KeyDown(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                button_login_Click(null, null);
            }
        }

        private void Accessing_Load(object sender, EventArgs e)
        {
            //textBox_username.Text = "tranngocnhan";
            //textBox_password.Text = "123";
           
        }

        private void linkLabel_forgetpassword_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MessageBox.Show("Liên hệ ___ để được cấp lại mật khẩu", "Quên mật khẩu", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (completed)
            {
                timer1.Enabled = false;
                panel1.Enabled = true;
                pictureBox1.Visible = label3.Visible = false;

                while (pictureBox2.Height <60)
                {
                    pictureBox2.Height+=2;
                    if (Height> 245)
                    {
                        Height--;
                    }
                    Application.DoEvents();
                }
                textBox_username.Focus();
            }
            else if (thread == null)
            {
                panel1.Enabled = false;
                pictureBox1.Visible = true;
                thread = new Thread(ThreadStart);
                thread.TrySetApartmentState(ApartmentState.STA);
                thread.Start();
            }
        }

        Thread thread = null;
        public static bool completed = false;
        private void ThreadStart()
        {
            Program.db.Account.Where(x => x.UserName.ToLower().Equals("") && x.Password.Equals("")).FirstOrDefault();
            Accessing.completed = true;
        }

        private void Accessing_FormClosed(object sender, FormClosedEventArgs e)
        {
            timer1.Enabled = false;
            if (thread != null) thread.Abort();
        }
    }
}
