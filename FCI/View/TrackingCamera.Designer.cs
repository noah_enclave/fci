﻿namespace FCI.View
{
    partial class TrackingCamera
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TrackingCamera));
            this.image_PICBX = new System.Windows.Forms.PictureBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.listView_image = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox_listcourse = new System.Windows.Forms.ComboBox();
            this.comboBox_schoolyear = new System.Windows.Forms.ComboBox();
            this.textBox_gio = new System.Windows.Forms.TextBox();
            this.textBox_thungay = new System.Windows.Forms.TextBox();
            this.textBox_tiethoc = new System.Windows.Forms.TextBox();
            this.textBox_giobd = new System.Windows.Forms.TextBox();
            this.textBox_giokt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.image_PICBX)).BeginInit();
            this.SuspendLayout();
            // 
            // image_PICBX
            // 
            this.image_PICBX.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.image_PICBX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.image_PICBX.Location = new System.Drawing.Point(188, 27);
            this.image_PICBX.Name = "image_PICBX";
            this.image_PICBX.Size = new System.Drawing.Size(587, 430);
            this.image_PICBX.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.image_PICBX.TabIndex = 5;
            this.image_PICBX.TabStop = false;
            // 
            // listBox1
            // 
            this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(188, 482);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(587, 173);
            this.listBox1.TabIndex = 7;
            // 
            // listView_image
            // 
            this.listView_image.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView_image.Location = new System.Drawing.Point(781, 27);
            this.listView_image.MultiSelect = false;
            this.listView_image.Name = "listView_image";
            this.listView_image.Size = new System.Drawing.Size(164, 628);
            this.listView_image.TabIndex = 33;
            this.listView_image.UseCompatibleStateImageBehavior = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label1.Location = new System.Drawing.Point(18, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 15);
            this.label1.TabIndex = 36;
            this.label1.Text = "Lớp tín chỉ:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label5.Location = new System.Drawing.Point(18, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 15);
            this.label5.TabIndex = 37;
            this.label5.Text = "Năm học: ";
            // 
            // comboBox_listcourse
            // 
            this.comboBox_listcourse.DisplayMember = "CourseName";
            this.comboBox_listcourse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_listcourse.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.comboBox_listcourse.FormattingEnabled = true;
            this.comboBox_listcourse.Location = new System.Drawing.Point(18, 106);
            this.comboBox_listcourse.Name = "comboBox_listcourse";
            this.comboBox_listcourse.Size = new System.Drawing.Size(164, 23);
            this.comboBox_listcourse.TabIndex = 2;
            this.comboBox_listcourse.ValueMember = "idCourse";
            this.comboBox_listcourse.SelectedIndexChanged += new System.EventHandler(this.comboBox_listcourse_SelectedIndexChanged);
            // 
            // comboBox_schoolyear
            // 
            this.comboBox_schoolyear.DisplayMember = "CourseName";
            this.comboBox_schoolyear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_schoolyear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.comboBox_schoolyear.FormattingEnabled = true;
            this.comboBox_schoolyear.Location = new System.Drawing.Point(18, 49);
            this.comboBox_schoolyear.Name = "comboBox_schoolyear";
            this.comboBox_schoolyear.Size = new System.Drawing.Size(164, 23);
            this.comboBox_schoolyear.TabIndex = 1;
            this.comboBox_schoolyear.ValueMember = "idCourse";
            this.comboBox_schoolyear.SelectedIndexChanged += new System.EventHandler(this.comboBox_schoolyear_SelectedIndexChanged);
            // 
            // textBox_gio
            // 
            this.textBox_gio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.textBox_gio.Location = new System.Drawing.Point(18, 242);
            this.textBox_gio.Name = "textBox_gio";
            this.textBox_gio.ReadOnly = true;
            this.textBox_gio.Size = new System.Drawing.Size(164, 21);
            this.textBox_gio.TabIndex = 7;
            this.textBox_gio.Text = "Thời Gian: _";
            // 
            // textBox_thungay
            // 
            this.textBox_thungay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.textBox_thungay.Location = new System.Drawing.Point(18, 269);
            this.textBox_thungay.Name = "textBox_thungay";
            this.textBox_thungay.ReadOnly = true;
            this.textBox_thungay.Size = new System.Drawing.Size(164, 21);
            this.textBox_thungay.TabIndex = 8;
            this.textBox_thungay.Text = "Thứ 3, 01/12/2017";
            // 
            // textBox_tiethoc
            // 
            this.textBox_tiethoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.textBox_tiethoc.Location = new System.Drawing.Point(18, 148);
            this.textBox_tiethoc.Name = "textBox_tiethoc";
            this.textBox_tiethoc.ReadOnly = true;
            this.textBox_tiethoc.Size = new System.Drawing.Size(130, 21);
            this.textBox_tiethoc.TabIndex = 3;
            this.textBox_tiethoc.Text = "Tiết học: _";
            // 
            // textBox_giobd
            // 
            this.textBox_giobd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.textBox_giobd.Location = new System.Drawing.Point(18, 175);
            this.textBox_giobd.Name = "textBox_giobd";
            this.textBox_giobd.ReadOnly = true;
            this.textBox_giobd.Size = new System.Drawing.Size(164, 21);
            this.textBox_giobd.TabIndex = 5;
            this.textBox_giobd.Text = "Giờ bắt đầu:  _";
            // 
            // textBox_giokt
            // 
            this.textBox_giokt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.textBox_giokt.Location = new System.Drawing.Point(18, 202);
            this.textBox_giokt.Name = "textBox_giokt";
            this.textBox_giokt.ReadOnly = true;
            this.textBox_giokt.Size = new System.Drawing.Size(164, 21);
            this.textBox_giokt.TabIndex = 6;
            this.textBox_giokt.Text = "Giờ kết thúc: _";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label2.Location = new System.Drawing.Point(185, 460);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 15);
            this.label2.TabIndex = 37;
            this.label2.Text = "Lịch sử: ";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 5000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 668);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(957, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button1.Image = global::FCI.Properties.Resources.camera1;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(18, 321);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(164, 42);
            this.button1.TabIndex = 0;
            this.button1.Text = "Bật Camera";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.Location = new System.Drawing.Point(154, 148);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(28, 21);
            this.button2.TabIndex = 4;
            this.button2.Text = "?";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // TrackingCamera
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = global::FCI.Properties.Resources.bk;
            this.ClientSize = new System.Drawing.Size(957, 690);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.textBox_gio);
            this.Controls.Add(this.textBox_thungay);
            this.Controls.Add(this.textBox_giokt);
            this.Controls.Add(this.textBox_giobd);
            this.Controls.Add(this.textBox_tiethoc);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBox_listcourse);
            this.Controls.Add(this.comboBox_schoolyear);
            this.Controls.Add(this.listView_image);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.image_PICBX);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TrackingCamera";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Điểm danh bằng Camera";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Tracking_Camera_FormClosed);
            this.Load += new System.EventHandler(this.Tracking_Camera_Load);
            ((System.ComponentModel.ISupportInitialize)(this.image_PICBX)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox image_PICBX;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ListView listView_image;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBox_listcourse;
        private System.Windows.Forms.ComboBox comboBox_schoolyear;
        private System.Windows.Forms.TextBox textBox_gio;
        private System.Windows.Forms.TextBox textBox_thungay;
        private System.Windows.Forms.TextBox textBox_tiethoc;
        private System.Windows.Forms.TextBox textBox_giobd;
        private System.Windows.Forms.TextBox textBox_giokt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}