﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AForge.Video.DirectShow;
using Emgu.CV;

namespace FCI.View.Control
{
    public partial class CameraSetting : UserControl
    {
        private bool DeviceExist = false;
        private FilterInfoCollection videoDevices;
        public Capture cap;
        public CameraSetting()
        {
            InitializeComponent();
        }

        private void CameraSetting_Load(object sender, EventArgs e)
        {
            getCamList();
        }
        private void getCamList()
        {
            try
            {
                videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
                comboBox1.Items.Clear();
                if (videoDevices.Count == 0)
                    throw new ApplicationException();

                DeviceExist = true;
                for (int i = videoDevices.Count - 1; i >= 0; i--)
                {
                    comboBox1.Items.Add(videoDevices[i].Name);
                }
                if (Data.US.cameraID >= comboBox1.Items.Count)
                {
                    comboBox1.SelectedIndex = 0;
                }
                else
                {
                    comboBox1.SelectedIndex = Data.US.cameraID; //make dafault to first cam
                }
            }
            catch (ApplicationException)
            {
                DeviceExist = false;
                comboBox1.Items.Add("No capture device on your system");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            stopCamera();
            getCamList();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (timer1.Enabled)
            {
                stopCamera();
                button3.Text = "Kiểm tra";
                return;
            }

            if (!DeviceExist)
            {
                MessageBox.Show("Không thể mở Camera.", "Lỗi");
                return;
            }

            try
            {
                cap = new Capture(comboBox1.SelectedIndex);
                cap.QueryFrame();
                cap.FlipHorizontal = true;
                timer1.Enabled = true;
                button3.Text = "Tắt Camera";
            }
            catch
            {
                MessageBox.Show("Không thể mở Camera.", "Lỗi");
            }

        }

        private void stopCamera()
        {
            timer1.Enabled = false;
            pictureBox1.Image = null;
            if (cap != null) cap.Dispose();
            cap = null;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (cap == null || cap.QueryFrame() == null) return;
            pictureBox1.Image = cap.QueryFrame().ToBitmap();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Data.US.saveChange(-1, -1, comboBox1.SelectedIndex))
            {
                MessageBox.Show("Lưu thành công", "Thông báo");
            }
            else
            {
                MessageBox.Show("Không thể lưu CameraID", "Thông báo");
            }

        }
    }
}
