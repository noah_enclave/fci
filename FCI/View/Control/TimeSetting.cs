﻿using System;
using System.Windows.Forms;

namespace FCI.View.Control
{
    public partial class TimeSetting : UserControl
    {
        public TimeSetting()
        {
            InitializeComponent();
        }

        private int? check0(int inp)
        {
            if (inp < 0) return null;
            return inp;
        }

        private void TimeSetting_Load(object sender, EventArgs e)
        {
            numericUpDown_early.Value = check0(Data.US.minEarly) ?? 30;
            numericUpDown_late.Value = check0(Data.US.minLate) ?? 5;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (numericUpDown_late.Value > 39)
            {
                MessageBox.Show("Thời gian cho phép đi trễ không hợp lệ.", "Thông báo");
                return;
            }
            if (Data.US.saveChange(inp_minLate: int.Parse(numericUpDown_late.Value.ToString()), inp_minEarly: int.Parse(numericUpDown_early.Value.ToString())))
            {
                MessageBox.Show("Lưu thành công", "Thông báo");
            }
            else
            {
                MessageBox.Show("Không thể lưu thông số", "Thông báo");
            }
        }
    }
}
