﻿using System;
using System.Windows.Forms;

namespace FCI.View.Control
{
    public partial class DetectParam : UserControl
    {
        public DetectParam()
        {
            InitializeComponent();
        }
        private int? check0(int inp)
        {
            if (inp < 0) return null;
            return inp;
        }
        private void DetectParam_Load(object sender, EventArgs e)
        {
            numericUpDown_threshold.Value = check0(Data.US.threshold) ?? 70;
            numericUpDown_numCap.Value = check0(Data.US.numCap) ?? 10;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Data.US.saveChange(inp_threshold: int.Parse(numericUpDown_threshold.Value.ToString()), inp_numCap: int.Parse(numericUpDown_numCap.Value.ToString())))
            {
                MessageBox.Show("Lưu thành công", "Thông báo");
            }
            else
            {
                MessageBox.Show("Không thể lưu thông số", "Thông báo");
            }
        }
    }
}
