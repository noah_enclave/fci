﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FCI.View
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            if (e.CloseReason == CloseReason.WindowsShutDown) return;
            if (Data.Session.idAccount == -1)
            {
                Program.db.SaveChanges();
            }
            else
            {
                switch (MessageBox.Show(this, "Bạn muốn Thoát chương trình?\nTất cả dữ liệu sẽ được lưu lại.", "Thông báo", MessageBoxButtons.YesNo))
                {
                    case DialogResult.No:
                        e.Cancel = true;
                        break;
                    default:
                        Program.db.SaveChanges();
                        break;
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool callMDI(string tag)
        {
            panel_intro.Visible = false;
            foreach (Form f in this.MdiChildren)
            {
                if (f.Tag.ToString() == tag)
                {
                    f.WindowState = FormWindowState.Maximized;
                    f.Activate();
                    return true;
                }
            }
            return false;
        }
        private void button_tracking_Click(object sender, EventArgs e)
        {

            if (TrackingCamera.opening)
            {
                Program.trackingcamera.Activate();
                return;
            }
            (Program.trackingcamera = new TrackingCamera()).Show();
        }

        private void button_learning_Click(object sender, EventArgs e)
        {
            if (callMDI("learning")) return;
            (new Learning { Tag = "learning", MdiParent = this , WindowState = FormWindowState.Maximized }).Show();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (callMDI("tracking")) return;
            (new Tracking { Tag = "tracking", MdiParent = this, WindowState = FormWindowState.Maximized }).Show();
        }
        private void Main_Load(object sender, EventArgs e)
        {
            MdiClient chld;
            foreach (System.Windows.Forms.Control ctrl in this.Controls)
            {
                try
                {
                    chld = (MdiClient)ctrl;
                    chld.BackColor = this.BackColor;
                }
                catch { }
            }
            init_session();
        }

        private void init_session()
        {
            var I = (from ins in Program.db.Instructor
                     join acc in Program.db.Account on ins.I_idAccount equals acc.idAccount
                     where acc.idAccount == Data.Session.idAccount
                     select new
                     {
                         ins.idInstructor,
                         ins.I_Name,
                         ins.I_idDepartment
                     }).FirstOrDefault();
            if (I == null)
            {
                Data.Session.setInstructor("admin", "System Admin", "admin");
            }
            else
            {
                Data.Session.setInstructor(I.idInstructor, I.I_Name, I.I_idDepartment);
            }
            Data.Session.RoleName = Program.db.Role.FirstOrDefault(r => r.idRole == Data.Session.idRole).RoleName;
            var dpm = Program.db.Department.FirstOrDefault(d => d.idDepartment == Data.Session.I_idDepartment);
            Data.Session.DepartmentName = dpm == null ? "" : dpm.DepartmentName;
            textBox_session.Text = Data.Session.getSession();
            button5.Visible = !(2 <= Data.Session.idRole && Data.Session.idRole <= 7);
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {

            switch (MessageBox.Show(this, "Bạn muốn Đăng xuất tài khoản?\nTất cả dữ liệu sẽ được lưu lại.", "Thông báo", MessageBoxButtons.YesNo))
            {
                case DialogResult.Yes:
                    Program.db.SaveChanges();
                    Data.Session.clearSession();
                    textBox_session.Text = "";
                    Program.trackingcamera.Close();
                    foreach (Form f in this.MdiChildren) f.Close();
                    panel_intro.Visible = true;
                    Enabled = false;
                    Enabled = Program.login(false);
                    if (Enabled) init_session();
                    break;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            (new SystemSetting()).ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (callMDI("usermanaging")) return;
            (new UserManaging { Tag = "usermanaging", MdiParent = this , WindowState = FormWindowState.Maximized }).Show();
        }
    }
}
