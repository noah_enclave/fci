﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using FCI.Data;
using FCI.Func;
using FCI.Struct;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace FCI.View
{
    public partial class Learning : Form
    {
        #region Variables

        //Images for finding face
        Image<Bgr, byte> currentFrame;
        Image<Gray, byte> result = null;
        Image<Gray, byte> gray_frame = null;
        
        private IEnumerable<Data.Student> studentList;
        private DataManager dmgr = new DataManager();
        private ImageList imageList = new ImageList();
        private List<ImageReal> imageRealList = new List<ImageReal>();
        private int timerCount = 5;
        #endregion
        public Learning()
        {
            InitializeComponent();
        }

        //Camera Start Stop
        public void init_capture()
        {
            Program.grabber = new Capture(US.cameraID);
            Program.grabber.QueryFrame();
            Program.grabber.FlipHorizontal = true;
            Application.Idle += new EventHandler(FrameGrabber);
        }
        private void stop_capture()
        {
            Application.Idle -= new EventHandler(FrameGrabber);
            if (Program.grabber != null)
            {
                Program.grabber.Dispose();
                Program.grabber = null;
            }
        }
        //Process Frame
        void FrameGrabber(object sender, EventArgs e)
        {
            //Get the current frame form capture device
            currentFrame = Program.grabber.QueryFrame().Resize(1, INTER.CV_INTER_CUBIC);

            //Convert it to Grayscale
            if (currentFrame != null)
            {
                gray_frame = currentFrame.Convert<Gray, byte>();

                //Face Detector
                var facesDetected = Program.faceLib.DetectMultiScale(gray_frame, 1.2, 10, new Size(50, 50), new Size(500, 500));

                foreach (var face_found in facesDetected)
                {
                    result = currentFrame.Copy(face_found).Convert<Gray, byte>().Resize(200, 200, INTER.CV_INTER_CUBIC);
                    result._EqualizeHist();
                    face_PICBX.Image = result.ToBitmap();
                    //draw the face detected in the 0th (gray) channel with blue color
                    currentFrame.Draw(face_found, new Bgr(Color.Red), 1);
                    break;
                }
                image_PICBX.Image = currentFrame.ToBitmap();
            }
        }
        //Saving The Data
        private bool save_training_data(Image face_data)
        {
            try
            {
                Random rand = new Random();
                string idUser = "_" + textBox_idStudent.Text;
                string fileName = "fci" + idUser + "_" + rand.Next().ToString() + ".jpg";
                while (File.Exists(Program.faceDir + fileName))
                {
                    fileName = "fci" + idUser + "_" + rand.Next().ToString() + ".jpg";
                }
                face_data.Save(Program.faceDir + fileName, ImageFormat.Jpeg);
                dmgr.uploadUserData(idUser, fileName);
                init_imageList(idUser);
                var a = Program.db.Student.FirstOrDefault(x => x.idStudent == textBox_idStudent.Text);
                if (a != null) a.existData = true;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }
        private ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }


        private void Learning_FormClosed(object sender, FormClosedEventArgs e)
        {
            stop_capture();
        }

        private void button_startCamera_Click(object sender, EventArgs e)
        {
            Enabled = false;
            if (Program.grabber == null)
            {
                try
                {
                    init_capture();
                }
                catch
                {
                    MessageBox.Show("Không thể khởi động Camera!\nHãy vào phần thiết lập hệ thống để cài đặt lại.");
                    Enabled = true;
                    return;
                }
                button_startCamera.Text = "Tắt Camera";
            }
            else
            {
                stop_capture();
                button_startCamera.Text = "Bật Camera";
            }
            Enabled = true;
        }


        private void Learning_Load(object sender, EventArgs e)
        {

            //
            stop_capture();

            // Setting data grid
            dataGridView_liststudent.AutoGenerateColumns = false;
            dataGridView_liststudent.AllowUserToResizeRows = false;
            dataGridView_liststudent.RowTemplate.Height = 30;

            // Setting image list
            imageList.ImageSize = new Size(55, 55);
            listView_image.LargeImageList = imageList;
            listView_image.View = System.Windows.Forms.View.Tile;
            //
            dateTimePicker_s_dob.Text = "";
            //init_sampledata();
            loadYear();

        }

        private void loadYear()
        {

            comboBox_schoolyear.Items.AddRange(F.yearSchool());
            var t = DateTime.Now.Month;
            var n = DateTime.Now.Year;
            string sy = "";
            if (t < 8) sy = (n - 1) + "-" + n;
            else sy = n + "-" + (n + 1);
            Console.WriteLine(sy);
            comboBox_schoolyear.SelectedItem = sy;
        }

        private void init_sampledata()
        {
            dataGridView_liststudent.DataSource = studentList = Program.db.Student.ToList();
        }
        private void comboBox_schoolyear_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridView_liststudent.DataSource = null;
            init_choise(false);
            init_comboboxCourse();
        }

        private void comboBox_listcourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            init_gridviewStudent();
        }
        private void init_comboboxCourse()
        {
            comboBox_listcourse.DataSource = (from cou in Program.db.Course
                                              join ins in Program.db.Instructor on cou.idInstructor equals ins.idInstructor
                                              join acc in Program.db.Account on ins.I_idAccount equals acc.idAccount
                                              where acc.idAccount == Data.Session.idAccount && cou.SchoolYear == comboBox_schoolyear.Text
                                              select new { cou.idCourse, cou.CourseName, cou.SchoolYear }).ToList();
        }

        private void init_gridviewStudent()
        {
            dataGridView_liststudent.DataSource = studentList = (from stu in Program.db.Student
                                                                 join sc in Program.db.STUDENT_COURSE on stu.idStudent equals sc.idStudent
                                                                 join cou in Program.db.Course on sc.idCourse equals cou.idCourse
                                                                 where cou.idCourse == comboBox_listcourse.SelectedValue.ToString() && sc.SchoolYear == cou.SchoolYear
                                                                 select stu).ToList();

            var count = studentList.Where(x => x.existData == true).Count();
            textBox1.Text = "SV đã có dữ liệu / tổng SV: " + count + " / " + studentList.Count();
        }

        private void dataGridView_liststudent_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView_liststudent.SelectedCells.Count > 0)
            {
                if (!checkBox_capnhatdulieu.Enabled) init_choise();
                var _idStudent = dataGridView_liststudent.Rows[e.RowIndex].Cells["Column_idStudent"].Value.ToString();
                var sv = (from sv1 in studentList where sv1.idStudent == _idStudent select sv1).FirstOrDefault();
                init_imageList("_" + sv.idStudent);
                init_form(sv);
            }
        }

        private void init_choise(bool set = true)
        {
            panel_chinhsua.Enabled = set;
            button_thayavatar.Enabled = button_xoaavatar.Enabled = set;
            checkBox_capnhatdulieu.Enabled = set;
            dateTimePicker_s_dob.CustomFormat = set ? "dd/MM/yyyy" : " ";
            if (!set)
            {
                textBox_idStudent.Text = "";
                textBox_s_name.Text = "";
                textBox_s_email.Text = "";
                textBox_s_phone.Text = "";
                face_PICBX.Image = null;
                image_PICBX.Image = null;
                imageList.Images.Clear();
                imageRealList.Clear();
                listView_image.Items.Clear();
                pictureBox_avatar.Image = null;
            }
        }

        private void init_form(Data.Student sv)
        {
            textBox_s_name.Text = sv.S_Name;
            dateTimePicker_s_dob.Value = sv.S_Dob;
            textBox_s_email.Text = sv.S_Email;
            textBox_s_phone.Text = sv.S_Phone;
            textBox_idStudent.Text = sv.idStudent;
            pictureBox_avatar.Image = F.byteArray2Image(sv.S_Avatar);
            face_PICBX.Image = null;
            image_PICBX.Image = null;
        }

        private void init_imageList(string idUser)
        {
            var listUserData = (from usd in Program.db.UserData
                                where usd.idUser == idUser
                                select usd).ToList();
            imageList.Images.Clear();
            imageRealList.Clear();
            foreach (var userData in listUserData)
            {
                var data = F.byteArray2Image(userData.Data);
                imageList.Images.Add(userData.FileName, data);
                imageRealList.Add(new ImageReal(userData.FileName, data));
            }
            button_deleteAll.Enabled = button_deleteImage.Enabled = imageList.Images.Count > 0;
            dataGridView_liststudent.Refresh();
            refreshListViewImage();
        }

        private void refreshListViewImage()
        {
            listView_image.Clear();
            for (int i = 0; i < imageList.Images.Count; i++)
            {
                listView_image.AddRow(i, (i + 1).ToString());
            }
        }




        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            panel_button.Enabled = checkBox_capnhatdulieu.Checked;
            if (checkBox_capnhatdulieu.Checked)
            {
                face_PICBX.Image = null;
            }
            else
            {
                image_PICBX.Image = null;
                stop_capture();
            }
        }

        private void listView_image_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView_image.SelectedItems.Count < 1 || imageList.Images.Count == 0) return;
            face_PICBX.Image = imageRealList.ElementAt(listView_image.SelectedIndices[0]).data;
        }

        private void button_addImage_Click(object sender, EventArgs e)
        {
            if (face_PICBX.Image == null && image_PICBX.Image == null)
            {
                MessageBox.Show("Dữ liệu khuôn mặt chưa sẵn sàng.");
                return;
            }

            if (!save_training_data(face_PICBX.Image)) MessageBox.Show("Error", "Lỗi lưu dữ liệu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else re_init_list();
        }

        private void re_init_list()
        {
            var index = dataGridView_liststudent.SelectedRows[0].Index;
            init_gridviewStudent();
            if (dataGridView_liststudent.Rows.Count > index)
                dataGridView_liststudent.CurrentCell = dataGridView_liststudent.Rows[index].Cells[1];
        }

        private void button_deleteImage_Click(object sender, EventArgs e)
        {
            if (listView_image.SelectedItems.Count != 1)
            {
                MessageBox.Show("Hãy chọn một hình ảnh ở DS hình ảnh");
                return;
            }
            if (MessageBox.Show("Xóa hình ảnh đã chọn?", "Xóa hình ảnh", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                var idUser = "_" + textBox_idStudent.Text;
                var fileName = imageRealList.ElementAt(listView_image.SelectedIndices[0]).fileName;
                if (dmgr.deleteUserData(idUser, fileName))
                {
                    init_imageList(idUser);
                    face_PICBX.Image = null;
                    if (imageList.Images.Count == 0)
                    {
                        var a = Program.db.Student.FirstOrDefault(x => x.idStudent == textBox_idStudent.Text);
                        if (a != null) a.existData = false;
                        re_init_list();
                    }
                }
            }
        }

        private void button_saveChange_Click(object sender, EventArgs e)
        {
            string iName = textBox_s_name.Text;
            string iEmail = textBox_s_email.Text;
            string iPhone = textBox_s_phone.Text;

            if (F.checkName(ref iName)) return;
            if (F.checkEmail(iEmail)) return;
            if (F.checkPhone(ref iPhone)) return;

            if (DateTime.Compare(DateTime.Now, dateTimePicker_s_dob.Value) <= 0)
            {
                MessageBox.Show("Ngày sinh không hợp lệ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (MessageBox.Show("Cập nhật những thông tin sinh viên?", "Cập nhật thông tin", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                var stu = Program.db.Student.Where(x => x.idStudent == textBox_idStudent.Text).FirstOrDefault();
                if (stu == null) return;
                stu.S_Name = iName;
                stu.S_Email = iEmail;
                stu.S_Phone = iPhone;
                if (dateTimePicker_s_dob.Text != "") stu.S_Dob = dateTimePicker_s_dob.Value;
                dataGridView_liststudent.Refresh();
            }
        }

        private void button_deleteAll_Click(object sender, EventArgs e)
        {
            if (dataGridView_liststudent.SelectedCells.Count == 0) return;
            if (MessageBox.Show("Xóa tất cả hình ảnh của Sinh Viên?", "Xóa hình ảnh", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                if (dmgr.deleteAllUserData("_" + textBox_idStudent.Text))
                {
                    init_imageList("_" + textBox_idStudent.Text);
                    var a = Program.db.Student.FirstOrDefault(x => x.idStudent == textBox_idStudent.Text);
                    if (a != null) a.existData = false;
                    face_PICBX.Image = null;
                    re_init_list();
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            now = DateTime.Now;
            if (--timerCount < 0)
            {
                timer1.Enabled = timer2.Enabled = label_count.Visible = false;
                re_init_list();
                return;
            }
            save_training_data(face_PICBX.Image);
        }

        private void button_them5hinh_Click(object sender, EventArgs e)
        {
            if (face_PICBX.Image == null && image_PICBX.Image == null)
            {
                MessageBox.Show("Dữ liệu khuôn mặt chưa sẵn sàng.");
                return;
            }
            if (MessageBox.Show("Chụp liên tiếp 5 hình ảnh trong 10 giây.\nHãy thay đổi vị trí khuôn mặt và đảm bảo chương trình nhận dạng được ở hình ảnh xem trước\nChọn Yes để bắt đầu", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                now = DateTime.Now;
                timerCount = 5;
                timer1.Enabled = timer2.Enabled = label_count.Visible = true;
            }
        }
        private DateTime now;

        private void timer2_Tick(object sender, EventArgs e)
        {
            label_count.Text = "(" + (timerCount) + ")\n" + (20 - Math.Round((DateTime.Now - now).TotalMilliseconds / 100, 0));
        }

        private void button_thayavatar_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Chọn Avatar";
                dlg.Filter = "Định dạng hỗ trợ|*.bmp;*.jpg;*.png|Tất cả tệp|*.*";
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    var stu = Program.db.Student.FirstOrDefault(x => x.idStudent == textBox_idStudent.Text);
                    if (stu == null) return;
                    pictureBox_avatar.Image = new Bitmap(dlg.FileName);
                    stu.S_Avatar = F.image2ByteArray(pictureBox_avatar.Image);
                }
            }
        }

        private void button_xoaavatar_Click(object sender, EventArgs e)
        {
            if (pictureBox_avatar.Image == null) return;
            if (MessageBox.Show("Xóa Avatar của sinh viên?", "Thông báo", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                var stu = Program.db.Student.FirstOrDefault(x => x.idStudent == textBox_idStudent.Text);
                if (stu == null) return;
                stu.S_Avatar = null;
                pictureBox_avatar.Image = null;
            }
        }
    }
}