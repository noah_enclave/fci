﻿namespace FCI.View
{
    partial class Learning
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Learning));
            this.comboBox_listcourse = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView_liststudent = new System.Windows.Forms.DataGridView();
            this.Column_existData = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column_S_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_idStudent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox_schoolyear = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.panel_button = new System.Windows.Forms.Panel();
            this.label_count = new System.Windows.Forms.Label();
            this.button_startCamera = new System.Windows.Forms.Button();
            this.button_them5hinh = new System.Windows.Forms.Button();
            this.button_addImage = new System.Windows.Forms.Button();
            this.button_deleteAll = new System.Windows.Forms.Button();
            this.button_deleteImage = new System.Windows.Forms.Button();
            this.checkBox_capnhatdulieu = new System.Windows.Forms.CheckBox();
            this.image_PICBX = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.face_PICBX = new System.Windows.Forms.PictureBox();
            this.listView_image = new System.Windows.Forms.ListView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button_xoaavatar = new System.Windows.Forms.Button();
            this.button_thayavatar = new System.Windows.Forms.Button();
            this.pictureBox_avatar = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel_chinhsua = new System.Windows.Forms.Panel();
            this.button_saveChange = new System.Windows.Forms.Button();
            this.textBox_s_phone = new System.Windows.Forms.TextBox();
            this.textBox_idStudent = new System.Windows.Forms.TextBox();
            this.dateTimePicker_s_dob = new System.Windows.Forms.DateTimePicker();
            this.textBox_s_name = new System.Windows.Forms.TextBox();
            this.textBox_s_email = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_liststudent)).BeginInit();
            this.panel2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel_button.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.image_PICBX)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.face_PICBX)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_avatar)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel_chinhsua.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBox_listcourse
            // 
            this.comboBox_listcourse.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_listcourse.DisplayMember = "CourseName";
            this.comboBox_listcourse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_listcourse.FormattingEnabled = true;
            this.comboBox_listcourse.Location = new System.Drawing.Point(87, 49);
            this.comboBox_listcourse.Name = "comboBox_listcourse";
            this.comboBox_listcourse.Size = new System.Drawing.Size(152, 23);
            this.comboBox_listcourse.TabIndex = 2;
            this.comboBox_listcourse.ValueMember = "idCourse";
            this.comboBox_listcourse.SelectedIndexChanged += new System.EventHandler(this.comboBox_listcourse_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label1.Location = new System.Drawing.Point(14, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Lớp tín chỉ:";
            // 
            // dataGridView_liststudent
            // 
            this.dataGridView_liststudent.AllowUserToAddRows = false;
            this.dataGridView_liststudent.AllowUserToDeleteRows = false;
            this.dataGridView_liststudent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_liststudent.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView_liststudent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_liststudent.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column_existData,
            this.Column_S_Name,
            this.Column_idStudent});
            this.dataGridView_liststudent.Location = new System.Drawing.Point(6, 82);
            this.dataGridView_liststudent.MultiSelect = false;
            this.dataGridView_liststudent.Name = "dataGridView_liststudent";
            this.dataGridView_liststudent.ReadOnly = true;
            this.dataGridView_liststudent.RowHeadersVisible = false;
            this.dataGridView_liststudent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView_liststudent.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_liststudent.Size = new System.Drawing.Size(233, 401);
            this.dataGridView_liststudent.TabIndex = 0;
            this.dataGridView_liststudent.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_liststudent_RowEnter);
            // 
            // Column_existData
            // 
            this.Column_existData.DataPropertyName = "existData";
            this.Column_existData.FalseValue = "False";
            this.Column_existData.HeaderText = "";
            this.Column_existData.Name = "Column_existData";
            this.Column_existData.ReadOnly = true;
            this.Column_existData.TrueValue = "True";
            this.Column_existData.Width = 30;
            // 
            // Column_S_Name
            // 
            this.Column_S_Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column_S_Name.DataPropertyName = "S_Name";
            this.Column_S_Name.FillWeight = 200F;
            this.Column_S_Name.HeaderText = "Họ Tên";
            this.Column_S_Name.Name = "Column_S_Name";
            this.Column_S_Name.ReadOnly = true;
            // 
            // Column_idStudent
            // 
            this.Column_idStudent.DataPropertyName = "idStudent";
            this.Column_idStudent.HeaderText = "";
            this.Column_idStudent.Name = "Column_idStudent";
            this.Column_idStudent.ReadOnly = true;
            this.Column_idStudent.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel2.Controls.Add(this.groupBox5);
            this.panel2.Controls.Add(this.groupBox4);
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Location = new System.Drawing.Point(12, 21);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(993, 551);
            this.panel2.TabIndex = 5;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox5.Controls.Add(this.textBox1);
            this.groupBox5.Controls.Add(this.dataGridView_liststudent);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.comboBox_listcourse);
            this.groupBox5.Controls.Add(this.comboBox_schoolyear);
            this.groupBox5.Location = new System.Drawing.Point(20, 18);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(245, 518);
            this.groupBox5.TabIndex = 36;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Danh sách lớp";
            // 
            // textBox1
            // 
            this.textBox1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textBox1.Location = new System.Drawing.Point(6, 489);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(233, 21);
            this.textBox1.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label5.Location = new System.Drawing.Point(18, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 15);
            this.label5.TabIndex = 3;
            this.label5.Text = "Năm học: ";
            // 
            // comboBox_schoolyear
            // 
            this.comboBox_schoolyear.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_schoolyear.DisplayMember = "CourseName";
            this.comboBox_schoolyear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_schoolyear.FormattingEnabled = true;
            this.comboBox_schoolyear.Location = new System.Drawing.Point(87, 19);
            this.comboBox_schoolyear.Name = "comboBox_schoolyear";
            this.comboBox_schoolyear.Size = new System.Drawing.Size(152, 23);
            this.comboBox_schoolyear.TabIndex = 1;
            this.comboBox_schoolyear.ValueMember = "idCourse";
            this.comboBox_schoolyear.SelectedIndexChanged += new System.EventHandler(this.comboBox_schoolyear_SelectedIndexChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox4.Controls.Add(this.panel_button);
            this.groupBox4.Controls.Add(this.checkBox_capnhatdulieu);
            this.groupBox4.Controls.Add(this.image_PICBX);
            this.groupBox4.Location = new System.Drawing.Point(271, 255);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(503, 281);
            this.groupBox4.TabIndex = 35;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Camera";
            // 
            // panel_button
            // 
            this.panel_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_button.Controls.Add(this.label_count);
            this.panel_button.Controls.Add(this.button_startCamera);
            this.panel_button.Controls.Add(this.button_them5hinh);
            this.panel_button.Controls.Add(this.button_addImage);
            this.panel_button.Controls.Add(this.button_deleteAll);
            this.panel_button.Controls.Add(this.button_deleteImage);
            this.panel_button.Enabled = false;
            this.panel_button.Location = new System.Drawing.Point(382, 54);
            this.panel_button.Name = "panel_button";
            this.panel_button.Size = new System.Drawing.Size(110, 202);
            this.panel_button.TabIndex = 31;
            // 
            // label_count
            // 
            this.label_count.BackColor = System.Drawing.Color.Gray;
            this.label_count.Font = new System.Drawing.Font("Arial", 29F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label_count.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label_count.Location = new System.Drawing.Point(0, 0);
            this.label_count.Name = "label_count";
            this.label_count.Size = new System.Drawing.Size(110, 202);
            this.label_count.TabIndex = 35;
            this.label_count.Text = " ";
            this.label_count.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_count.Visible = false;
            // 
            // button_startCamera
            // 
            this.button_startCamera.Location = new System.Drawing.Point(3, 3);
            this.button_startCamera.Name = "button_startCamera";
            this.button_startCamera.Size = new System.Drawing.Size(104, 30);
            this.button_startCamera.TabIndex = 12;
            this.button_startCamera.Text = "Bật Camera";
            this.button_startCamera.UseVisualStyleBackColor = true;
            this.button_startCamera.Click += new System.EventHandler(this.button_startCamera_Click);
            // 
            // button_them5hinh
            // 
            this.button_them5hinh.Location = new System.Drawing.Point(3, 77);
            this.button_them5hinh.Name = "button_them5hinh";
            this.button_them5hinh.Size = new System.Drawing.Size(104, 30);
            this.button_them5hinh.TabIndex = 14;
            this.button_them5hinh.Text = "Chụp 5 hình";
            this.button_them5hinh.UseVisualStyleBackColor = true;
            this.button_them5hinh.Click += new System.EventHandler(this.button_them5hinh_Click);
            // 
            // button_addImage
            // 
            this.button_addImage.Location = new System.Drawing.Point(3, 41);
            this.button_addImage.Name = "button_addImage";
            this.button_addImage.Size = new System.Drawing.Size(104, 30);
            this.button_addImage.TabIndex = 13;
            this.button_addImage.Text = "Chụp 1 hình";
            this.button_addImage.UseVisualStyleBackColor = true;
            this.button_addImage.Click += new System.EventHandler(this.button_addImage_Click);
            // 
            // button_deleteAll
            // 
            this.button_deleteAll.Location = new System.Drawing.Point(3, 149);
            this.button_deleteAll.Name = "button_deleteAll";
            this.button_deleteAll.Size = new System.Drawing.Size(104, 30);
            this.button_deleteAll.TabIndex = 16;
            this.button_deleteAll.Text = "Xóa DS hình";
            this.button_deleteAll.UseVisualStyleBackColor = true;
            this.button_deleteAll.Click += new System.EventHandler(this.button_deleteAll_Click);
            // 
            // button_deleteImage
            // 
            this.button_deleteImage.Location = new System.Drawing.Point(3, 113);
            this.button_deleteImage.Name = "button_deleteImage";
            this.button_deleteImage.Size = new System.Drawing.Size(104, 30);
            this.button_deleteImage.TabIndex = 15;
            this.button_deleteImage.Text = "Xóa hình ảnh";
            this.button_deleteImage.UseVisualStyleBackColor = true;
            this.button_deleteImage.Click += new System.EventHandler(this.button_deleteImage_Click);
            // 
            // checkBox_capnhatdulieu
            // 
            this.checkBox_capnhatdulieu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_capnhatdulieu.AutoSize = true;
            this.checkBox_capnhatdulieu.Enabled = false;
            this.checkBox_capnhatdulieu.Location = new System.Drawing.Point(382, 29);
            this.checkBox_capnhatdulieu.Name = "checkBox_capnhatdulieu";
            this.checkBox_capnhatdulieu.Size = new System.Drawing.Size(117, 19);
            this.checkBox_capnhatdulieu.TabIndex = 11;
            this.checkBox_capnhatdulieu.Text = "Cập nhật dữ liệu";
            this.checkBox_capnhatdulieu.UseVisualStyleBackColor = true;
            this.checkBox_capnhatdulieu.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // image_PICBX
            // 
            this.image_PICBX.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.image_PICBX.BackColor = System.Drawing.SystemColors.ControlDark;
            this.image_PICBX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.image_PICBX.Location = new System.Drawing.Point(6, 20);
            this.image_PICBX.Name = "image_PICBX";
            this.image_PICBX.Size = new System.Drawing.Size(370, 249);
            this.image_PICBX.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.image_PICBX.TabIndex = 16;
            this.image_PICBX.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox3.Controls.Add(this.face_PICBX);
            this.groupBox3.Controls.Add(this.listView_image);
            this.groupBox3.Location = new System.Drawing.Point(791, 18);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(182, 518);
            this.groupBox3.TabIndex = 34;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "DS hình ảnh";
            // 
            // face_PICBX
            // 
            this.face_PICBX.BackColor = System.Drawing.SystemColors.ControlDark;
            this.face_PICBX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.face_PICBX.Location = new System.Drawing.Point(8, 20);
            this.face_PICBX.Name = "face_PICBX";
            this.face_PICBX.Size = new System.Drawing.Size(166, 166);
            this.face_PICBX.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.face_PICBX.TabIndex = 17;
            this.face_PICBX.TabStop = false;
            // 
            // listView_image
            // 
            this.listView_image.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView_image.Location = new System.Drawing.Point(8, 192);
            this.listView_image.MultiSelect = false;
            this.listView_image.Name = "listView_image";
            this.listView_image.Size = new System.Drawing.Size(166, 314);
            this.listView_image.TabIndex = 17;
            this.listView_image.UseCompatibleStateImageBehavior = false;
            this.listView_image.SelectedIndexChanged += new System.EventHandler(this.listView_image_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox2.Controls.Add(this.button_xoaavatar);
            this.groupBox2.Controls.Add(this.button_thayavatar);
            this.groupBox2.Controls.Add(this.pictureBox_avatar);
            this.groupBox2.Location = new System.Drawing.Point(598, 17);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(187, 232);
            this.groupBox2.TabIndex = 33;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Ảnh đại diện";
            // 
            // button_xoaavatar
            // 
            this.button_xoaavatar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_xoaavatar.Location = new System.Drawing.Point(10, 195);
            this.button_xoaavatar.Name = "button_xoaavatar";
            this.button_xoaavatar.Size = new System.Drawing.Size(59, 23);
            this.button_xoaavatar.TabIndex = 10;
            this.button_xoaavatar.Text = "Xóa";
            this.button_xoaavatar.UseVisualStyleBackColor = true;
            this.button_xoaavatar.Click += new System.EventHandler(this.button_xoaavatar_Click);
            // 
            // button_thayavatar
            // 
            this.button_thayavatar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_thayavatar.Location = new System.Drawing.Point(89, 195);
            this.button_thayavatar.Name = "button_thayavatar";
            this.button_thayavatar.Size = new System.Drawing.Size(87, 23);
            this.button_thayavatar.TabIndex = 9;
            this.button_thayavatar.Text = "Thay đổi";
            this.button_thayavatar.UseVisualStyleBackColor = true;
            this.button_thayavatar.Click += new System.EventHandler(this.button_thayavatar_Click);
            // 
            // pictureBox_avatar
            // 
            this.pictureBox_avatar.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pictureBox_avatar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_avatar.InitialImage = global::FCI.Properties.Resources.no_avatar;
            this.pictureBox_avatar.Location = new System.Drawing.Point(10, 23);
            this.pictureBox_avatar.Name = "pictureBox_avatar";
            this.pictureBox_avatar.Size = new System.Drawing.Size(166, 166);
            this.pictureBox_avatar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_avatar.TabIndex = 17;
            this.pictureBox_avatar.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Controls.Add(this.panel_chinhsua);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(271, 17);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(321, 232);
            this.groupBox1.TabIndex = 31;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin Sinh Viên";
            // 
            // panel_chinhsua
            // 
            this.panel_chinhsua.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_chinhsua.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel_chinhsua.Controls.Add(this.button_saveChange);
            this.panel_chinhsua.Controls.Add(this.textBox_s_phone);
            this.panel_chinhsua.Controls.Add(this.textBox_idStudent);
            this.panel_chinhsua.Controls.Add(this.dateTimePicker_s_dob);
            this.panel_chinhsua.Controls.Add(this.textBox_s_name);
            this.panel_chinhsua.Controls.Add(this.textBox_s_email);
            this.panel_chinhsua.Enabled = false;
            this.panel_chinhsua.Location = new System.Drawing.Point(102, 21);
            this.panel_chinhsua.Name = "panel_chinhsua";
            this.panel_chinhsua.Size = new System.Drawing.Size(213, 176);
            this.panel_chinhsua.TabIndex = 34;
            // 
            // button_saveChange
            // 
            this.button_saveChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_saveChange.Location = new System.Drawing.Point(111, 145);
            this.button_saveChange.Name = "button_saveChange";
            this.button_saveChange.Size = new System.Drawing.Size(93, 23);
            this.button_saveChange.TabIndex = 8;
            this.button_saveChange.Text = "Lưu thay đổi";
            this.button_saveChange.UseVisualStyleBackColor = true;
            this.button_saveChange.Click += new System.EventHandler(this.button_saveChange_Click);
            // 
            // textBox_s_phone
            // 
            this.textBox_s_phone.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_s_phone.Location = new System.Drawing.Point(3, 115);
            this.textBox_s_phone.Name = "textBox_s_phone";
            this.textBox_s_phone.Size = new System.Drawing.Size(201, 21);
            this.textBox_s_phone.TabIndex = 7;
            // 
            // textBox_idStudent
            // 
            this.textBox_idStudent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_idStudent.Location = new System.Drawing.Point(3, 6);
            this.textBox_idStudent.Name = "textBox_idStudent";
            this.textBox_idStudent.ReadOnly = true;
            this.textBox_idStudent.Size = new System.Drawing.Size(201, 21);
            this.textBox_idStudent.TabIndex = 3;
            // 
            // dateTimePicker_s_dob
            // 
            this.dateTimePicker_s_dob.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePicker_s_dob.CustomFormat = " ";
            this.dateTimePicker_s_dob.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_s_dob.Location = new System.Drawing.Point(3, 60);
            this.dateTimePicker_s_dob.Name = "dateTimePicker_s_dob";
            this.dateTimePicker_s_dob.Size = new System.Drawing.Size(201, 21);
            this.dateTimePicker_s_dob.TabIndex = 5;
            this.dateTimePicker_s_dob.Value = new System.DateTime(2017, 5, 2, 1, 29, 12, 0);
            // 
            // textBox_s_name
            // 
            this.textBox_s_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_s_name.Location = new System.Drawing.Point(3, 33);
            this.textBox_s_name.Name = "textBox_s_name";
            this.textBox_s_name.Size = new System.Drawing.Size(201, 21);
            this.textBox_s_name.TabIndex = 4;
            // 
            // textBox_s_email
            // 
            this.textBox_s_email.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_s_email.Location = new System.Drawing.Point(3, 86);
            this.textBox_s_email.Name = "textBox_s_email";
            this.textBox_s_email.Size = new System.Drawing.Size(201, 21);
            this.textBox_s_email.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(30, 138);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 15);
            this.label9.TabIndex = 1;
            this.label9.Text = "Điện thoại:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(31, 84);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 15);
            this.label7.TabIndex = 1;
            this.label7.Text = "Ngày sinh:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(54, 111);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 15);
            this.label8.TabIndex = 1;
            this.label8.Text = "Email:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 15);
            this.label4.TabIndex = 21;
            this.label4.Text = "Mã Sinh Viên:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(46, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 15);
            this.label6.TabIndex = 1;
            this.label6.Text = "Họ Tên:";
            // 
            // timer1
            // 
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // Learning
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1029, 584);
            this.Controls.Add(this.panel2);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Learning";
            this.Text = "Danh sách";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Learning_FormClosed);
            this.Load += new System.EventHandler(this.Learning_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_liststudent)).EndInit();
            this.panel2.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.panel_button.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.image_PICBX)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.face_PICBX)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_avatar)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel_chinhsua.ResumeLayout(false);
            this.panel_chinhsua.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox_listcourse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView_liststudent;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button_deleteImage;
        private System.Windows.Forms.TextBox textBox_idStudent;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button_addImage;
        private System.Windows.Forms.PictureBox face_PICBX;
        private System.Windows.Forms.PictureBox image_PICBX;
        private System.Windows.Forms.Button button_startCamera;
        private System.Windows.Forms.ComboBox comboBox_schoolyear;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_s_name;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox_s_email;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox_s_phone;
        private System.Windows.Forms.DateTimePicker dateTimePicker_s_dob;
        private System.Windows.Forms.ListView listView_image;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox checkBox_capnhatdulieu;
        private System.Windows.Forms.Panel panel_button;
        private System.Windows.Forms.Button button_saveChange;
        private System.Windows.Forms.Panel panel_chinhsua;
        private System.Windows.Forms.Button button_deleteAll;
        private System.Windows.Forms.Button button_them5hinh;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label_count;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.PictureBox pictureBox_avatar;
        private System.Windows.Forms.Button button_xoaavatar;
        private System.Windows.Forms.Button button_thayavatar;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column_existData;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_S_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_idStudent;
    }
}