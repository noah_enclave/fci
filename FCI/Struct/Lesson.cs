﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FCI.Struct
{
    public class LessonDate
    {
        public DateTime lessonDate;
        private string[] dow = { "Chủ Nhật", "Thứ 2", "Thứ 3", "Thứ 4", "Thứ 5", "Thứ 6", "Thứ 7" };
        public LessonDate(DateTime? d)
        {
            lessonDate = d ?? new DateTime(1970, 1, 1);

        }
        public string getDate()
        {
            return lessonDate.ToString("dd/MM/yyy");
        }
        public string getDay()
        {
            return dow[(int)lessonDate.DayOfWeek];
        }
        public override string ToString()
        {
            return getDay() + ", " + getDate();
        }
    }

    class LessonTime
    {
        private string lessonTimeStart;
        private string lessonTimeEnd;
        public LessonTime(string t = "")
        {
            lessonTimeStart = t.Split('-')[0];
            lessonTimeEnd = t.Split('-')[1];
        }
        public string getHour()
        {
            return lessonTimeStart.Split(':')[0];
        }
        public string getMinute()
        {
            return lessonTimeStart.Split(':')[1];
        }
        public string getFullTime()
        {
            return lessonTimeStart + '-' + lessonTimeEnd;
        }
        public string getWhen()
        {
            int h = int.Parse(getHour());
            int m = int.Parse(getMinute());
            if (h < 12) return "Sáng";
            if (h < 17) return "Chiều";
            return "Tối";
        }
        public override string ToString()
        {
            return lessonTimeStart + "-" + lessonTimeEnd;
        }
        public string getLessonTimeStart()
        {
            return lessonTimeStart;
        }
        public string getLessonTimeEnd()
        {
            return lessonTimeEnd;
        }
    }
    class Lesson
    {
        public int index;
        public LessonTime time;
        public LessonDate date;
        public DateTime fullTimeStart;
        public DateTime fullTimeEnd;
        public Lesson() { }
        public Lesson(int i, DateTime d, string t)
        {
            index = i;
            date = new LessonDate(d);
            time = new LessonTime(t);
            fullTimeStart = DateTime.Parse(date.lessonDate.ToString("yyyy/MM/dd") + " " + time.getLessonTimeStart() + ":00.000");
            fullTimeEnd = DateTime.Parse(date.lessonDate.ToString("yyyy/MM/dd") + " " + time.getLessonTimeEnd() + ":00.000");
        }

        public override string ToString()
        {
            // index + buoi + time + time end
            return date.ToString();
        }
        public string[] ToArray()
        {
            return new string[] { index.ToString(), time.getWhen(), time.getLessonTimeStart(), "", time.getLessonTimeEnd() };
        }
    }
}
