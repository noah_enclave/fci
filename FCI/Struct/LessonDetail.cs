﻿namespace FCI.Struct
{
    class LessonDetail
    {
        public Data.Student student;
        public Lesson lesson;
        public string vangtong;
        public string muonhoc;
        public string giodiemdanh;
        public System.Drawing.Image image;
        public System.Windows.Forms.ListViewItem item;

    }
}
