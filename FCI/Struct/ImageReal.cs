﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace FCI.Struct
{
    class ImageReal
    {
        public string fileName = "";
        public Image data = null;
        public ImageReal(string f, Image d)
        {
            fileName = f; data = d;
        }
    }
}
