﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Drawing;
using System.Drawing.Imaging;

namespace FCI.Func
{
    class DataManager
    {
        // Variables
        private JObject jsonObject = null;

        public DataManager()
        {
            if (!Directory.Exists(Program.faceDir))
            {
                Directory.CreateDirectory(Program.faceDir);
            }
            init_jsonObject();
        }

        private void init_jsonObject()
        {
            var jsonString = File.Exists(Program.faceIndexFile) ? File.ReadAllText(Program.faceIndexFile) : "{}";
            jsonObject = JObject.Parse(jsonString); 
        }

        private bool save_jsonObject()
        {
            if (jsonObject == null) return false;
            File.WriteAllText(Program.faceIndexFile, jsonObject.ToString());
            return true;
        }

        private void jsonObjectInsertFileName(string idUser, string fileName, bool saveChange = true)
        {// if idUser key not exist, create new
            JArray arr = new JArray();
            if ((JToken)jsonObject[idUser]!=null) arr = jsonObject[idUser].ToObject<JArray>();
            arr.Add(fileName);
            jsonObject[idUser] = arr;
            if (saveChange) save_jsonObject();
        }


        private bool jsonObjectDeleteFileName(string idUser, string fileName, bool saveChange = true)
        {// just delete value, not delete key
            if ((JToken)jsonObject[idUser] == null) return false;
            JArray arr = jsonObject[idUser].ToObject<JArray>();
            arr.Remove(fileName);
            jsonObject[idUser] = arr;
            if (saveChange) save_jsonObject();
            return true;
        }

        private bool jsonObjectDeleteUserDate(string idUser, bool saveChange = true)
        {
            if ((JToken)jsonObject[idUser] == null) return false;
            jsonObject[idUser] = new JArray();
            if (saveChange) save_jsonObject();
            return true;
        }

        public bool uploadUserData(string idUser, string fileName)
        {// had on local, func for server
            try {
                var userDataRecord = new Data.UserData();
                userDataRecord.idUser = idUser;
                userDataRecord.FileName = fileName;
                userDataRecord.Data = File.ReadAllBytes(Program.faceDir + fileName);
                Program.db.UserData.Add(userDataRecord);
                Program.db.SaveChanges();
                jsonObjectInsertFileName(idUser, fileName);
                return true;
            }catch(Exception)
            {
                return false;
                //System.Windows.Forms.MessageBox.Show(ex.ToString());
            }
       }

        public void downloadUserData(string idUser)
        {// had on server, func for local
            var listUserData = (from usd in Program.db.UserData
                                where usd.idUser == idUser
                                select usd).ToList();
            foreach (var userData in listUserData)
            {
                if (!File.Exists(Program.faceDir + userData.FileName))
                {
                    using (Image image = Image.FromStream(new MemoryStream(userData.Data)))
                    {
                        image.Save(Program.faceDir + userData.FileName, ImageFormat.Jpeg);
                    }
                    jsonObjectInsertFileName(userData.idUser, userData.FileName, false);
                }
            }
            save_jsonObject();
        }

        public bool deleteUserData(string idUser, string fileName)
        {// working on server and local

            try {
                // Server side
                var res = Program.db.UserData.Where(x => x.idUser == idUser && x.FileName == fileName).FirstOrDefault();
                if (res != null)
                {
                    Program.db.UserData.Remove(res);
                    Program.db.SaveChanges();
                }
                // Local side
                if (File.Exists(Program.faceDir + fileName))
                {
                    File.Delete(Program.faceDir + fileName);
                }
                jsonObjectDeleteFileName(idUser, fileName);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool deleteAllUserData(string idUser)
        {
            try
            {
                var res = Program.db.UserData.Where(x => x.idUser == idUser).ToList();
                if (res != null)
                {
                    foreach(var ud in res)
                    {
                        if (File.Exists(Program.faceDir + ud.FileName))
                        {
                            File.Delete(Program.faceDir + ud.FileName);
                        }
                        Program.db.UserData.Remove(ud);
                    }
                    jsonObjectDeleteUserDate(idUser);
                    Program.db.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
