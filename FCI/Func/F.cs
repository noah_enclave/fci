﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace FCI.Func
{
    static class F
    {

        private static string mark = "qwertyuioplkjhgfdsazxcvbnm";
        private static string symbol = "`1234567890-=~!@#$%^&*()_+;',./:<>?[]{}|\\\"";

        public static void AddRow(this ListView lvw, int image_index, string item_title, params string[] subitem_titles)
        {
            ListViewItem new_item = lvw.Items.Add(item_title, 1);
            new_item.ImageIndex = image_index;
        }

        public static void ExportToExcel(this DataTable dt, string excelFilePath = null, Data.Course course = null, Data.Student student = null)
        {
            try
            {
                if (dt == null || dt.Columns.Count == 0)
                    throw new Exception("ExportToExcel: Null or empty input table!\n");

                int space = 5;

                // load excel, and create a new workbook
                var excelApp = new Excel.Application();
                excelApp.Workbooks.Add();

                // single worksheet
                Excel._Worksheet workSheet = excelApp.ActiveSheet;

                // column headings
                for (var i = 0; i < dt.Columns.Count; i++)
                {
                    workSheet.Cells[1 + space, i + 1] = dt.Columns[i].ColumnName;
                }

                // rows
                for (var i = 0; i < dt.Rows.Count; i++)
                {
                    // to do: format datetime values before printing
                    for (var j = 0; j < dt.Columns.Count; j++)
                    {
                        workSheet.Cells[i + 2 + space, j + 1] = dt.Rows[i][j];
                        if (dt.Rows[i][j].ToString() == "Vắng")
                        {

                        }
                    }
                }

                // check file path
                if (!string.IsNullOrEmpty(excelFilePath))
                {
                    try
                    {
                        workSheet.SaveAs(excelFilePath);
                        excelApp.Quit();
                        MessageBox.Show("Lưu thành công!");
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Không thể lưu tệp.\n" + ex.Message);
                    }
                }
                else
                { // no file path is given
                    excelApp.Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ExportToExcel: \n" + ex.Message);
            }
        }
        public static Image byteArray2Image(byte[] data)
        {
            if (data == null || data.Length == 0) return null;
            using (var ms = new MemoryStream(data))
            {
                return Image.FromStream(ms);
            }
        }

        public static byte[] image2ByteArray(Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }

        public static string ConvertToUnsign3(string str)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = str.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }

        public static IEnumerable<DateTime> EachDay(DateTime beginDate, DateTime endDate, int step = 1)
        {
            for (var day = beginDate.Date; day.Date <= endDate.Date; day = day.AddDays(step))
                yield return day;
        }

        public static object[] yearSchool()
        {
            var sy = Program.db.Course.ToList().GroupBy(test => test.SchoolYear).Select(grp => grp.First());
            var yearSchool = new object[sy.Count()];
            for (int i = 0; i < sy.Count(); i++) yearSchool[i] = sy.ElementAt(i).SchoolYear;
            return yearSchool;
        }


        public static bool checkExist(int idAccount, string username)
        {
            var has = Program.db.Account.FirstOrDefault(a =>
                a.idAccount != idAccount && a.UserName == username);
            if (has == null) return false;
            MessageBox.Show("Tài khoản đã tồn tại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            return true;
        }
        public static bool checkUserName(string username)
        {
            mark += mark.ToUpper();
            mark += "1234567890_";
            foreach (char c in username)
            {
                if (!mark.Contains(c))
                {
                    MessageBox.Show("Tên tài khoản phải thuộc các ký tự\n[a-z],[A-Z],[0-9], '_'", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return true;
                }
            }
            return false;
        }
        public static bool checkPassword(string password, string password2)
        {
            if (password != password2)
            {
                MessageBox.Show("Mật khẩu không khớp", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return true;
            }
            if (password.Length < 6 || password.Length > 20)
            {
                MessageBox.Show("Độ dài Mật khẩu phải từ 6-20 ký tự", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return true;
            }
            return false;
        }
        public static bool checkName(ref string iName)
        {
            iName = iName.Trim();
            while (iName.IndexOf("  ") >= 0)
            {
                iName = iName.Replace("  ", " ");
            }
            if (iName.Length < 3 || iName.Length > 30)
            {
                MessageBox.Show("Tên không chính xác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return true;
            }
            foreach (char c in symbol)
            {
                if (iName.Contains(c))
                {
                    MessageBox.Show("Tên không được chứa ký tự đặc biệt", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return true;
                }
            }
            return false;
        }
        public static bool checkEmail(string iEmail)
        {
            if (iEmail == "") return false;
            mark += mark.ToUpper();
            mark += "1234567890_";
            var part = iEmail.Split('@');
            if (part.Length != 2 || iEmail.IndexOf("@") > iEmail.LastIndexOf("."))
            {
                MessageBox.Show("Email không hợp lệ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return true;
            }
            var markE = mark += "@.";
            foreach (char c in iEmail)
            {
                if (!mark.Contains(c))
                {
                    MessageBox.Show("Email không hợp lệ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return true;
                }
            }
            return false;
        }

        public static bool checkPhone(ref string iPhone)
        {
            if (iPhone == "") return false;
            iPhone = iPhone.Trim();
            while (iPhone.IndexOf("  ") >= 0)
            {
                iPhone = iPhone.Replace("  ", " ");
            }
            if (iPhone.Replace(" ", "").Length > 13 || iPhone.Replace(" ", "").Length < 9)
            {
                MessageBox.Show("Số điện thoại không đúng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return true;
            }
            var markP = "1234567890+ ";
            foreach (char c in iPhone)
            {
                if (!markP.Contains(c))
                {
                    MessageBox.Show("Số điện thoại không đúng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return true;
                }
            }
            return false;
        }

    }
}
