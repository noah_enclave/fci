﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.Structure;

using System.IO;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;

namespace FCI.Func
{
    class Classifier_Trained : IDisposable
    {

        #region Variables

        //Eigen
        private MCvTermCriteria termCrit;
        private FaceRecognizer recognizer;

        //training variables
        private List<Image<Gray, byte>> trainingImages = new List<Image<Gray, byte>>();//Images
        private List<string> Names_List = new List<string>(); //labels
        private List<int> Names_List_num = new List<int>();

        private int ContTrain, NumLabels;
        private double _distance = 0;
        private string _label;

        //Class Variables
        private bool _IsTrained = false;
        private string defaultName = "";


        private IEnumerable<Data.Student> studentList = null;

        #endregion

        public Classifier_Trained(IEnumerable<Data.Student> studentList = null, string Training_Folder = null)
        {
            this.studentList = studentList;
            termCrit = new MCvTermCriteria(ContTrain, 0.001);
            _IsTrained = LoadTrainingData(Training_Folder ?? Program.faceDir);
        }

        #region Public

        public bool IsTrained
        {
            get { return _IsTrained; }
        }

        public string Recognise(Image<Gray, byte> Input_image, int Eigen_Thresh = -1)
        {
            if (_IsTrained)
            {
                FaceRecognizer.PredictionResult ER = recognizer.Predict(Input_image);
                if (ER.Label == -1) Console.WriteLine(ER.Label);
                else Console.WriteLine(Names_List.ElementAt(ER.Label) + " - " + ER.Distance);
                if (ER.Label == -1)
                {
                    _label = defaultName;
                    _distance = 0;
                    return _label;
                }
                {
                    _label = Names_List.ElementAt(ER.Label);
                    _distance = ER.Distance;
                    if (_distance <= Data.US.threshold) return _label;
                    return defaultName;
                }

            }
            else return defaultName;
        }
        public double Get_Distance
        {
            get
            {
                //get eigenDistance
                return _distance;
            }
        }

        public void Save_Eigen_Recogniser(string filename)
        {
            StringBuilder sb = new StringBuilder();

            (new XmlSerializer(typeof(EigenObjectRecognizer))).Serialize(new StringWriter(sb), recognizer);
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(sb.ToString());
            xDoc.Save(filename);
        }

        public void Dispose()
        {
            recognizer = null;
            trainingImages = null;
            Names_List = null;
            GC.Collect();
        }

        #endregion
        private bool LoadTrainingData(string Folder_location)
        {
            Console.WriteLine("----------------");
            if (!File.Exists(Folder_location + Program.faceIndex)) return false;
            try
            {
                Names_List.Clear();
                Names_List_num.Clear();
                trainingImages.Clear();
                int count = 0;
                JObject indexJson = JObject.Parse(File.ReadAllText(Folder_location + Program.faceIndex));

                foreach (var st in studentList)
                {
                    if ((JToken)indexJson["_" + st.idStudent] == null) continue;
                    JArray arr = indexJson["_" + st.idStudent].ToObject<JArray>();
                    foreach (var img in arr)
                    {
                        if (!File.Exists(Folder_location + img.ToString())) continue;
                        Names_List.Add(F.ConvertToUnsign3(st.S_Name) + "@" + st.idStudent);
                        Names_List_num.Add(count++);
                        trainingImages.Add(new Image<Gray, byte>(Folder_location + img.ToString()));
                        ++NumLabels;
                    }
                }
                ContTrain = NumLabels;
                if (trainingImages.ToArray().Length == 0) return false;
                //Eigen face recognizer
                // recognizer = new EigenObjectRecognizer(trainingImages.ToArray(), Names_List.ToArray(), 5000, ref termCrit); //5000 default
                // recognizer = new EigenFaceRecognizer(80, Eigen_threshold);
                // recognizer = new FisherFaceRecognizer(0, Eigen_threshold);
                recognizer = new LBPHFaceRecognizer(1, 8, 8, 8, double.MaxValue);
                recognizer.Train(trainingImages.ToArray(), Names_List_num.ToArray());
                Console.WriteLine(trainingImages.Count);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
        }
    }
}